;;;; Advent-of-code
;;;; by Michael Reis
;;;; <michael@mtreis86.com>

(defsystem :advent-of-code
  :version "2020"
  :author "Michael Reis"
  :license "AGPL3.0"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "debugging") ; From PAIP - See license LICENSE-PAIP
                 (:file "testing") ; From PCL - See license LICENSE-PCL
                 (:file "aoc2019" :depends-on ("debugging" "testing"))
                 (:file "aoc2020" :depends-on ("debugging" "testing"))
                 (:file "aoc2021")
                 (:file "aoc2022")
                 (:file "aoc2024"))))
  :description "Advent of Code Solutions")
