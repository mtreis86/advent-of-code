(defun advent1 (list)
  (apply #'+ list))
  
(defun advent1pt2 (list &optional vals-seen (current-total 0))
  (loop :for num :in list
        :do (incf current-total num)
            (if (member current-total vals-seen)
                (return-from advent2 current-total)
                (push current-total vals-seen))
        :finally (return (advent2 list vals-seen current-total))))
        
(defun advent2 (list)
  (loop :with twos = 0
        :with threes = 0
        :for entry :in list
        :do (let ((count (count-chars (format nil "~A" entry))))
              (loop :with found-two
                    :with found-three
                    :for sub :in count
                    :do (when (= (rest sub) 2)
                          (setf found-two t))
                        (when (= (rest sub) 3)
                          (setf found-three t))
                          :finally (progn (when found-two (incf twos))
                                          (when found-three (incf threes)))))
        :finally (return (* twos threes))))

(defun count-chars (seq)
  (loop :with char-counts = (cons (cons (aref seq 0) 0) nil)
        :for char :across seq
        :do (if (assoc char char-counts)
                (incf (rest (assoc char char-counts)))
                (setf char-counts (acons char 1 char-counts)))
        :finally (return char-counts)))
        
(defun count-identical-entries (seq1 seq2)
  (loop :for entry1 :across seq1
        :for entry2 :across seq2
        :counting (equalp entry1 entry2)
        :into count
        :finally (return count)))        
