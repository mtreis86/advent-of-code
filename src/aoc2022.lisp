(defpackage :aoc2022
  (:use :cl))

(in-package :aoc2022)


(defun with-file-per-line (path fn)
  (with-open-file (stream path)
    (loop for line = (read-line stream nil)
          while line
          do (funcall fn line))))


(defun 01a ()
  (let ((acc 0)
        (max 0))
    (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input1.txt"
      (lambda (line)
        (if (string= line "")
            (progn (when (> acc max)
                     (setf max acc))
                   (setf acc 0))
            (incf acc (parse-integer line)))))
    max))

(defun 01b ()
  (let ((acc 0)
        (max 0)
        (maxier 0)
        (maxiest 0))
    (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input1.txt"
      (lambda (line)
        (if (string= line "")
            (progn (cond ((>= acc maxiest)
                          (setf max maxier
                               maxier maxiest
                               maxiest acc))
                         ((>= acc maxier)
                          (setf max maxier
                                maxier acc))
                         ((> acc max)
                          (setf max acc)))
                   (setf acc 0))
            (incf acc (parse-integer line)))))
    (+ max maxier maxiest)))


(defmacro ors (test key &body alternatives)
  (loop for alt in alternatives
        collecting `(,test ,key ,alt) into result
        finally (return `(or ,@result))))

(defun 02a ()
  (let ((acc 0))
    (labels ((score (pair)
               (+ (ecase (outcome pair)
                    (loss 0)
                    (draw 3)
                    (won 6))
                  (ecase (cadr pair)
                    (rock 1)
                    (paper 2)
                    (scissors 3))))
             (outcome (pair)
               (cond ((winning pair)
                      'won)
                     ((drawing pair)
                      'draw)
                     ((losing pair)
                      'loss)
                     (t (error "Undecidable outcome"))))
             (winning (pair)
               (ors equalp pair
                 '(rock paper)
                 '(paper scissors)
                 '(scissors rock)))
             (drawing (pair)
               (ors equalp pair
                 '(rock rock)
                 '(paper paper)
                 '(scissors scissors)))
             (losing (pair)
               (ors equalp pair
                 '(rock scissors)
                 '(paper rock)
                 '(scissors paper)))
             (parse (line)
               (list (convert (char line 0))
                     (convert (char line 2))))
             (convert (char)
               (cond ((or (char= char #\A)
                          (char= char #\X))
                      'rock)
                     ((or (char= char #\B)
                          (char= char #\Y))
                      'paper)
                     ((or (char= char #\C)
                          (char= char #\Z))
                      'scissors))))
      (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input2.txt"
       (lambda (line)
         (incf acc (score (parse line))))))
    acc))


(defun 02b ()
  (let ((acc 0))
    (labels ((score (pair)
               (+ (ecase (outcome pair)
                    (loss 0)
                    (draw 3)
                    (won 6))
                  (ecase (cadr pair)
                    (rock 1)
                    (paper 2)
                    (scissors 3))))
             (outcome (pair)
               (cond ((winning pair)
                      'won)
                     ((drawing pair)
                      'draw)
                     ((losing pair)
                      'loss)
                     (t (error "Undecidable outcome"))))
             (winning (pair)
               (ors equalp pair
                 '(rock paper)
                 '(paper scissors)
                 '(scissors rock)))
             (drawing (pair)
               (ors equalp pair
                 '(rock rock)
                 '(paper paper)
                 '(scissors scissors)))
             (losing (pair)
               (ors equalp pair
                 '(rock scissors)
                 '(paper rock)
                 '(scissors paper)))
             (parse (line)
               (let ((hand (convert-hand (char line 0))))
                 (list hand (assess hand (char line 2)))))
             (convert-hand (char)
               (case char
                 (#\A 'rock)
                 (#\B 'paper)
                 (#\C 'scissors)))
             (assess (hand char)
               (case (convert-outcome char)
                 (won (case hand
                        (rock 'paper)
                        (paper 'scissors)
                        (scissors 'rock)))
                 (loss (case hand
                         (rock 'scissors)
                         (paper 'rock)
                         (scissors 'paper)))
                 (draw (case hand
                         (rock 'rock)
                         (paper 'paper)
                         (scissors 'scissors)))))
             (convert-outcome (char)
               (case char
                 (#\X 'loss)
                 (#\Y 'draw)
                 (#\Z 'won))))
      (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input2.txt"
       (lambda (line)
         (incf acc (score (parse line))))))
    acc))

(defun 03a ()
  (labels ((split-in-half (string)
             (let ((half-point (floor (length string) 2)))
               (list (subseq string 0 half-point)
                     (subseq string half-point))))
           (match-pairs (string-pair)
             (loop for char across (first string-pair)
                   until (find char (second string-pair))
                   finally (return char)))
           (prioritize (char)
             (let ((code (char-code char)))
               (cond ((and (>= code (char-code #\a))
                           (<= code (char-code #\z)))
                      (+ 1 (- code (char-code #\a))))
                     ((and (>= code (char-code #\A))
                           (<= code (char-code #\Z)))
                      (+ 27 (- code (char-code #\A))))
                     (t (error "Non-char: ~A" char))))))
    (let ((sum 0))
      (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input3.txt"
        (lambda (line)
          (incf sum (prioritize (match-pairs (split-in-half line))))))
      sum)))

(defun 03b ()
  (labels ((match-item (strings)
             (loop for char across (first strings)
                   until (and (find char (second strings))
                              (find char (third strings)))
                   finally (return char)))
           (prioritize (char)
             (let ((code (char-code char)))
               (cond ((and (>= code (char-code #\a))
                           (<= code (char-code #\z)))
                      (+ 1 (- code (char-code #\a))))
                     ((and (>= code (char-code #\A))
                           (<= code (char-code #\Z)))
                      (+ 27 (- code (char-code #\A))))
                     (t (error "Non-char: ~A" char))))))
    (with-open-file (stream #P"~/common-lisp/advent-of-code/src/2022/input3.txt")
      (loop with total = 0
            for line = (read-line stream nil)
            while line
            with count = 0
            with sacks = '()
            do (push line sacks)
            if (= count 2)
              do (setf count 0)
                 (incf total (prioritize (match-item sacks)))
            else do (incf count)
            finally (return total)))))

(defun 04a ()
  (labels ((range-overlap-p (r1a r1b r2a r2b)
             (or (and (>= r1a r2a)
                      (<= r1b r2b))
                 (and (<= r1a r2a)
                      (>= r1b r2b)))))
    (let ((count 0))
      (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input4.txt"
        (lambda (line)
          (multiple-value-bind (r1 r2)
              (split line #\,)
            (multiple-value-bind (r1a r1b)
                (split r1 #\-)
              (multiple-value-bind (r2a r2b)
                  (split r2 #\-)
                (when (range-overlap-p (parse-integer r1a) (parse-integer r1b)
                                       (parse-integer r2a) (parse-integer r2b))
                  (incf count)))))))
      count)))

(defun split (string delimiter)
  (let ((pos (position delimiter string)))
    (values (subseq string 0 pos)
            (subseq string (1+ pos)))))

(defun 04b ()
  (labels ((range-overlap-p (r1a r1b r2a r2b)
             (or (and (>= r1a r2a)
                      (<= r1a r2b))
                 (and (>= r2a r1a)
                      (<= r2a r1b))
                 (and (<= r1b r2b)
                      (>= r1b r2a))
                 (and (<= r2b r1b)
                      (>= r2b r1a)))))
    (let ((count 0))
      (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input4.txt"
        (lambda (line)
          (multiple-value-bind (r1 r2)
              (split line #\,)
            (multiple-value-bind (r1a r1b)
                (split r1 #\-)
              (multiple-value-bind (r2a r2b)
                  (split r2 #\-)
                (when (range-overlap-p (parse-integer r1a) (parse-integer r1b)
                                       (parse-integer r2a) (parse-integer r2b))
                  (incf count)))))))
      count)))


(defun 05a ()
  (labels ((init-stacks (init)
             (loop with stacks = (make-list (length (first (last init))))
                   for row in init
                   do (loop for crate in row
                            do (push (cdr crate) (nth (car crate) stacks)))
                   finally (return (loop for stack in stacks
                                         collecting (nreverse stack)))))
           (move-crates (moves stacks)
             (loop for move in moves do
                   (loop with count = (first move)
                         with from = (second move)
                         with to = (third move)
                         repeat count
                         do (let ((on-crane (pop (nth from stacks))))
                              (push on-crane (nth to stacks))
                             stacks))))
           (parse-move (line)
             (mvb* (((drop rest) (split line #\SPACE) (declare (ignore drop)))
                    ((count rest) (split rest #\SPACE))
                    ((drop rest) (split rest #\SPACE)(declare (ignore drop)))
                    ((from rest) (split rest #\SPACE))
                    ((drop to) (split rest #\SPACE)(declare (ignore drop))))
               (list (parse-integer count) (1- (parse-integer from)) (1- (parse-integer to)))))
           (top-of-lists (lists)
             (loop for list in lists
                   collecting (first list) into result
                   finally (return (coerce result 'string)))))
    (with-open-file (stream #P"~/common-lisp/advent-of-code/src/2022/input5.txt")
      (loop for line = (read-line stream nil)
            while line
            when (> (length line) 0)
              if (char= #\[ (char line 0))
                collecting (loop for char across line
                                 for index from 0
                                 when (alpha-char-p char)
                                   collecting (cons (floor (1- index) 4) char))
                  into stacks
            else if (char= #\m (char line 0))
                   collecting (parse-move line) into moves
            finally (return (top-of-lists (move-crates moves (init-stacks stacks))))))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun %build-mvb* (var-and-form-pairs body)
   (let* ((pair (first var-and-form-pairs))
          (first (first pair))
          (second (rest pair))
          (rest (rest var-and-form-pairs)))
    (if rest
        (let ((next (%build-mvb* (rest var-and-form-pairs) body)))
          `(multiple-value-bind ,first
               ,@second
             ,next))
        `(multiple-value-bind ,first
             ,@second
           ,@body)))))

(defmacro mvb* (var-and-form-pairs &body body)
  (%build-mvb* var-and-form-pairs body))

(defun 05b ()
  (declare (optimize debug))
  (labels ((init-stacks (init)
             (loop with stacks = (make-list (length (first (last init))))
                   for row in init
                   do (loop for crate in row
                            do (push (cdr crate) (nth (car crate) stacks)))
                   finally (return (loop for stack in stacks
                                         collecting (nreverse stack)))))
           (move-crates (moves stacks)
             (loop for move in moves
                   do (loop with count = (first move)
                            with from = (second move)
                            with to = (third move)
                            repeat count
                            collecting (pop (nth from stacks)) into crane
                            finally (loop for crate in (nreverse crane)
                                          do (push crate (nth to stacks)))))
             stacks)
           (parse-move (line)
             (mvb* (((drop rest) (split line #\SPACE) (declare (ignore drop)))
                    ((count rest) (split rest #\SPACE))
                    ((drop rest) (split rest #\SPACE)(declare (ignore drop)))
                    ((from rest) (split rest #\SPACE))
                    ((drop to) (split rest #\SPACE)(declare (ignore drop))))
               (list (parse-integer count) (1- (parse-integer from)) (1- (parse-integer to)))))
           (top-of-lists (lists)
             (loop for list in lists
                   collecting (first list) into result
                   finally (return (coerce result 'string)))))
    (with-open-file (stream #P"~/common-lisp/advent-of-code/src/2022/input5.txt")
      (loop for line = (read-line stream nil)
            while line
            when (> (length line) 0)
              if (char= #\[ (char line 0))
                collecting (loop for char across line
                                 for index from 0
                                 when (alpha-char-p char)
                                   collecting (cons (floor (1- index) 4) char))
                  into stacks
            else if (char= #\m (char line 0))
                   collecting (parse-move line) into moves
            finally (return (top-of-lists (move-crates moves (init-stacks stacks))))))))


(defun 06a ()
  (with-open-file (stream #P"~/common-lisp/advent-of-code/src/2022/input6.txt")
    (loop for char = (read-char stream nil)
          for index from 1
          with char1 ; prev
          with char2 ; prev of prev
          with char3 ; prev of prev of prev
          while char
          until (and char3 (uniquep (list char char1 char2 char3) #'char=))
          do (setf char3 char2
                   char2 char1
                   char1 char)
          finally (format t "~A ~A ~A ~A" char3 char2 char1 char)
             (return index))))

(defun uniquep (list &optional (test #'eql))
  (= (length list) (length (remove-duplicates list :test test))))

(defun 06b ()
  (with-open-file (stream #P"~/common-lisp/advent-of-code/src/2022/input6.txt")
    (loop with buffer = (make-array 14 :element-type 'character)
          for char = (read-char stream nil)
          for index from 1
          for buffer-index = (mod (1- index) 14)
          while char
          do (setf (aref buffer buffer-index) char)
          until (uniquep (coerce buffer 'list) #'char=)
          finally (print buffer) (return index))))

(defun 07a ()
  (declare (optimize debug))
  (let* ((root (make-dir :name "root"))
         (dir root)
         (total 0))
    (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input7.txt"
      (lambda (line)
        (setf dir (process-cmd (parse-line line) dir root))))
    (update-sizes root)
    (walk-dir (lambda (dir)
                (let ((size (dir-size dir)))
                  (when (< size 100000)
                    (incf total size))))
              root)
    total))

(defun 07b ()
  (let* ((root (make-dir :name "root"))
         (dir root)
         (dir-size-to-delete)
         (space-needed))
    (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input7.txt"
      (lambda (line)
        (setf dir (process-cmd (parse-line line) dir root))))
    (update-sizes root)
    (setf dir-size-to-delete (dir-size root)
          space-needed (- 30000000 (- 70000000 (dir-size root))))
    (walk-dir (lambda (dir)
                (let ((size (dir-size dir)))
                  (when (and (> size space-needed)
                             (< size dir-size-to-delete))
                    (setf dir-size-to-delete size))))
              root)
    dir-size-to-delete))

;; folders is a tree of directories
;; files is an alist of name . size
(defstruct dir name folders files parent size)

(defmethod print-object ((dir dir) stream)
  (format stream "DIR: ~A" (dir-name dir)))

(defun update-sizes (dir)
  (let ((size (reduce #'+ (dir-files dir) :key #'cdr)))
    (mapcar (lambda (folder)
              (incf size (update-sizes folder)))
            (dir-folders dir))
    (setf (dir-size dir) size)))


(defun walk-dir (fn dir)
  (funcall fn dir)
  (when (dir-folders dir)
    (mapcar (lambda (folder)
              (walk-dir fn folder))
            (dir-folders dir))))

(defun parse-line (string)
  (labels ((parse-command (string)
             (let ((cmd (k-intern (subseq string 2 4))))
               (list :cmd cmd (when (eq cmd :cd)
                                (subseq string 5)))))
           (parse-result (string)
             (multiple-value-bind (dir-or-size name)
                 (split string #\SPACE)
               (if (every #'digit-char-p dir-or-size)
                   (list :file name (parse-integer dir-or-size))
                   (list :dir name)))))
    (if (char= #\$ (char string 0))
        (parse-command string)
        (parse-result string))))

(defun k-intern (string)
  (intern (string-upcase string) :keyword))

(defun process-cmd (cmd dir root)
  (case (first cmd)
    (:cmd (case (second cmd)
            (:cd (cond ((string= "/" (third cmd))
                        root)
                       ((string= ".." (third cmd))
                        (dir-parent dir))
                       (t (let ((next-dir (find (third cmd) (dir-folders dir)
                                                :key #'dir-name :test #'string=)))
                            (if next-dir next-dir
                                (error "dir not found"))))))
            (:ls dir)))
    (:file (push (cons (second cmd) (third cmd))
                 (dir-files dir))
           dir)
    (:dir (push (make-dir :name (second cmd) :parent dir)
                (dir-folders dir))
          dir)))


(defun 08a ()
  (let ((path #P"~/common-lisp/advent-of-code/src/2022/input8.txt")
        (forest)
        (visible)
        (width)
        (height)
        (total 0))
    (with-open-file (stream path)
      (let ((list (loop for line = (read-line stream nil)
                        while line
                        collecting line)))
        (setf width (length (first list))
              height (length list)
              forest (make-array (list width height) :element-type '(integer 0 9))
              visible (make-array (list width height) :element-type 'bit :initial-element 0))
        (loop for line in list
              for y from 0
              do (loop for tree across line
                       for x from 0
                       do (setf (aref forest x y)
                                (- (char-code tree) (char-code #\0)))))))
    ;; from left
    (loop for x from 0 below width
          do (loop for y from 0 below height
                   for tree = (aref forest x y)
                   with tallest = -1
                   until (and (= tree 9)
                              (= tallest 9))
                   when (> tree tallest)
                     do (setf (aref visible x y) 1
                              tallest tree)))
    ;; from right
    (loop for x from 0 below width
          do (loop for y downfrom (1- height) to 0
                   for tree = (aref forest x y)
                   with tallest = -1
                   until (and (= tree 9)
                              (= tallest 9))
                   when (> tree tallest)
                     do (setf (aref visible x y) 1
                              tallest tree)))
    ;; from top
    (loop for y from 0 below height
          do (loop for x from 0 below width
                   for tree = (aref forest x y)
                   with tallest = -1
                   until (and (= tree 9)
                              (= tallest 9))
                   when (> tree tallest)
                     do (setf (aref visible x y) 1
                              tallest tree)))
    ;; from bottom
    (loop for y from 0 below height
          do (loop for x downfrom (1- width) to 0
                   for tree = (aref forest x y)
                   with tallest = -1
                   until (and (= tree 9)
                              (= tallest 9))
                   when (> tree tallest)
                     do (setf (aref visible x y) 1
                              tallest tree)))
    (loop for x from 0 below width
          do (loop for y from 0 below height
                   do (incf total (aref visible x y))))
    total))



(defun 08b ()
  (let ((path #P"~/common-lisp/advent-of-code/src/2022/input8.txt")
        (forest)
        (scores)
        (width)
        (height))
    (with-open-file (stream path)
      (let ((list (loop for line = (read-line stream nil)
                        while line
                        collecting line)))
        (setf width (length (first list))
              height (length list)
              forest (make-array (list width height) :element-type '(integer 0 9))
              scores (make-array (list width height)))
        (loop for line in list
              for y from 0
              do (loop for tree across line
                       for x from 0
                       do (setf (aref forest x y)
                                (- (char-code tree) (char-code #\0)))))))
    (labels ((calc-score (x y dir tree)
               (case dir
                 (:up (loop for ya from (1+ y) below height
                            counting 1 into count
                            until (>= (aref forest x ya) tree)
                            finally (return count)))
                 (:down (loop for ya downfrom (1- y) to 0
                              counting 1 into count
                              until (>= (aref forest x ya) tree)
                              finally (return count)))
                 (:right (loop for xa from (1+ x) below width
                             counting 1 into count
                              until (>= (aref forest xa y) tree)
                              finally (return count)))
                 (:left (loop for xa downfrom (1- x) to 0
                              counting 1 into count
                              until (>= (aref forest xa y) tree)
                              finally (return count))))))
      (loop for x from 0 below width
            do (loop for y from 0 below height
                     do (let* ((tree (aref forest x y))
                               (up (calc-score x y :up tree))
                               (down (calc-score x y :down tree))
                               (right (calc-score x y :right tree))
                               (left (calc-score x y :left tree))
                               (total (* up down right left)))
                          ;; (format t "Tree: ~A:~A Total: ~A   U:~A D:~A :R~A L~A~%"
                          ;;         x y total up down right left)
                          (setf (aref scores x y) total))))
      (loop with max = 0
            for x from 0 below width
            do (loop for y from 0 below height
                     for score = (aref scores x y)
                     when (> score max)
                     do (setf max score))
            finally (return max)))))


;; (defun 09a ()
;;   (let ((table (make-hash-table :test #'equalp)))
;;     (labels ((parse-line (line)
;;                (cons (k-intern (subseq line 0 1))
;;                      (parse-integer (subseq line 2))))
;;              (move-head (cmd pos)
;;                (let ((mult (case (car cmd)
;;                              (:U '(0 . 1))
;;                              (:D '(0 . -1))
;;                              (:L '(-1 . 0))
;;                              (:R '(1 . 0))))
;;                      (dis (cdr cmd)))
;;                  (cons (+ (car pos) (* dis (car mult)))
;;                        (+ (cdr pos) (* dis (cdr mult))))))
;;              (move-tail (head tail)
;;                ())
;;              (store (position)
;;                (setf (gethash position table) t)))
;;       (let ((head (cons 0 0)))
;;         (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input9.txt"
;;           (lambda (line)
;;             (setf head (move (parse-line line) head))))))
;;     (hash-table-count table)))



(defun 10a ()
  (declare (optimize debug))
  (let ((states (make-array 1 :adjustable t :fill-pointer t :initial-contents '(1))))
    (labels ((parse-line (line)
               (let ((delim (position #\SPACE line)))
                 (cons (k-intern (if delim
                                     (subseq line 0 delim)
                                     line))
                       (when delim
                         (parse-integer (subseq line (1+ delim)))))))
             (update-state (cmd-pair)
               (ecase (car cmd-pair)
                 (:addx (noop)
                        (addx (rest cmd-pair)))
                 (:noop (noop))))
             (noop ()
               (vector-push-extend (aref states (1- (fill-pointer states)))
                                   states))
             (addx (num)
               (vector-push-extend (+ num (aref states (1- (fill-pointer states))))
                                   states)))
      (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input10.txt"
        (lambda (line)
          (update-state (parse-line line)))))
    (labels ((signal-strength (cycle)
               (* cycle (aref states (1- cycle)))))
      (loop for cycle from 20 to 220 by 40
            summing (signal-strength cycle)))))

(defun 10b ()
  (declare (optimize debug))
  (let ((states (make-array 1 :adjustable t :fill-pointer t :initial-contents '(1))))
    (labels ((parse-line (line)
               (let ((delim (position #\SPACE line)))
                 (cons (k-intern (if delim
                                     (subseq line 0 delim)
                                     line))
                       (when delim
                         (parse-integer (subseq line (1+ delim)))))))
             (update-state (cmd-pair)
               (ecase (car cmd-pair)
                 (:addx (noop)
                        (addx (rest cmd-pair)))
                 (:noop (noop))))
             (noop ()
               (vector-push-extend (aref states (1- (fill-pointer states)))
                                   states))
             (addx (num)
               (vector-push-extend (+ num (aref states (1- (fill-pointer states))))
                                   states)))
      (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/input10.txt"
        (lambda (line)
          (update-state (parse-line line)))))
    (labels ((sprite (line index)
               (let ((state (aref states (+ (* line 40) index))))
                 (or (= state index)
                     (= state (1+ index))
                     (= state (1- index))))))
      (loop for line from 0 to 5
            do (loop with output = (make-string 40)
                     for index from 0 to 39
                     do (if (sprite line index)
                            (setf (char output index) #\#)
                            (setf (char output index) #\.))
                     finally (print output))))))


#|
Monkey 0:
Starting items: 79, 98
Operation: new = old * 19
Test: divisible by 23
If true: throw to monkey 2
If false: throw to monkey 3
|#

(defstruct monkey
  (items nil)
  (op '() :type list)
  (test 0 :type integer)
  (if-t 0 :type integer)
  (if-f 0 :type integer))

(defun 11a ()
  (let ((store (make-array 1 :adjustable t :fill-pointer 0)))
    (labels ((parse-line (line)
               (if (alpha-char-p (char line 0))
                   (vector-push-extend (make-monkey) store)
                   (let ((monkey (aref store (1- (fill-pointer store)))))
                     (case (char line 2)
                       (#\S (setf (monkey-items monkey) (parse-items (subseq line 18))))
                       (#\O (setf (monkey-op monkey) (parse-op (subseq line 23))))
                       (#\T (setf (monkey-test monkey) (parse-integer (subseq line 21))))
                       (otherwise (case (char line 7)
                                    (#\t (setf (monkey-if-t monkey)
                                               (parse-integer (subseq line 29))))
                                    (#\f (setf (monkey-if-f monkey)
                                               (parse-integer (subseq line 30))))))))))
             (parse-items (items)
               (loop with last-start = 0
                     for index from 0
                     for char across items
                     when (char= char #\,)
                     collecting (parse-integer (subseq items last-start index)) into result
                     and do (setf last-start (+ index 2))
                     finally (return (list-to-dll
                                      (append result
                                              (list (parse-integer (subseq items last-start))))))))
             (parse-op (op)
               (let ((fn (read-from-string op nil))
                     (arg (read-from-string op nil nil :start 2)))
                 `(lambda (old)
                    (,fn old ,arg)))))
      (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/test11.txt"
        (lambda (line)
          (when (> (length line) 0)
            (parse-line line)))))
    (loop for round from 1 to 1 ; 20
          do (loop for monkey across store
                   do (loop with prev = nil
                            for item = (pop-dll (monkey-items monkey))
                            until (eql prev item)
                            do (print item)
                               (setf prev item))))))
                      ;;       for worry = (floor (funcall (monkey-op item)) 3)
                      ;;       do (if (mod worry 23)
                      ;;              (push-dll worry (nth (monkey-if-t monkey) store))))
                   






      ;; (parse-line "Monkey 0:")
      ;; (parse-items "78, 79")
      ;; (parse-line "  Starting items: 79, 98")
      ;; (parse-line "  Operation: new = old * old")
      ;; (parse-line "  Test: divisible by 23")
      ;; (parse-line "    If true: throw to monkey 2")
      ;; (parse-line "    If false: throw to monkey 3")
      ;; (aref store 0))))







(defstruct (dll (:constructor %make-dll))
  (head nil)
  (obj nil)
  (tail nil))

(defun make-dll (obj)
  "Make a new Double Linked List cell."
  (%make-dll :obj obj))

(defmacro push-dll (obj dll &optional (head-or-tail :head))
  "Push a new object onto the double linked list at the :head or :tail. Modifies the list in place
  like push."
  (ecase head-or-tail
    (:head `(progn (setf (dll (%make-dll :obj ,obj :tail ,dll))
                         (dll-head (dll-tail dll) dll))
                   dll))
    (:tail `(progn (setf (dll-tail ,dll) (%make-dll :obj ,obj :head ,dll))
                   ,dll))))

(defmacro pop-dll (dll &optional (head-or-tail :head))
  "Pop an object off the :head or :tail of the double linked list, modifying the list in place,
  and return that object."
  (ecase head-or-tail
    (:head (if (symbolp dll)
               `(multiple-value-bind (obj head)
                    (pop-dll-head ,dll)
                  (setf ,dll head)
                  obj)
               `(pop-dll-head ,dll))) ; TODO I am not sure there aren't edge cases here
    (:tail `(pop-dll-tail dll))))

(defun pop-dll-tail (dll)
  (let* ((last (last-dll dll nil))
         (obj (dll-obj last))
         (new-tail (dll-head last)))
    (when new-tail ; dll has multiple cells
      (setf (dll-tail new-tail) nil)) ; cut off old tail
    obj))

(defun pop-dll-head (dll)
  (let* ((first (first-dll dll nil))
         (obj (dll-obj first))
         (new-head (dll-tail first)))
    (when new-head ; dll has multiple cells
      (setf (dll-head new-head) nil)) ; cut off old head
    (values obj new-head)))









(defun last-dll (dll &optional (fn #'dll-obj))
  "Return the last object in the DLL unless another fn specifies what to do with the last cell.
  In a circular list, returns the head of the given cell."
  (labels ((recur (next)
             (let ((tail (dll-tail next)))
               (cond ((eql tail dll) ; circular
                      (dll-head dll))
                     (tail
                      (recur tail))
                     (fn (funcall fn next))
                     (t next)))))
    (recur dll)))

(defun first-dll (dll &optional (fn #'dll-obj))
  "Return the first cell of the DLL unless fn specifies something else to do with the cell. In a
   circular list, returns the tail of the given cell."
  (labels ((recur (prev)
             (let ((head (dll-head prev)))
               (cond ((eql head dll) ; circular
                      (dll-tail dll))
                     (head
                      (recur head))
                     (fn (funcall fn prev))
                     (t prev)))))
    (recur dll)))

(defun inject-dll (obj dll)
  "Stick the new object into a new cell after the given DLL cell."
  (let* ((tail (dll-tail dll))
         (new (%make-dll :head dll :obj obj :tail tail)))
    (setf (dll-head tail) new
          (dll-tail dll) new)
    dll))

(defun nth-dll (n dll)
  "Return the DLL object at the given position from the first cell."
  (labels ((recur (n dll)
             (if (plusp n)
                 (nth-dll (1- n) dll)
                 dll)))
    (recur n (first-dll dll))))

(defun find-dll (obj dll &key (test #'eql) key)
  "Does the object exist, given the test and the key to access it, within the DLL?"
  (if (funcall test (if key
                        (funcall key (dll-obj dll))
                        (dll-obj dll)))
      dll
      (find-dll obj (dll-tail dll) :test test :key key)))

(defun list-to-dll (list)
  "Convert the list to a DLL."
  (let ((first (make-dll (first list))))
    (labels ((recur (dll list)
               (when list
                 (let ((new-cell (make-dll (first list))))
                   (setf (dll-tail dll) new-cell
                         (dll-head new-cell) dll))
                 (recur (dll-tail dll) (rest list)))))
      (recur first (rest list)))
    first))

(defmethod print-object ((dll dll) stream)
  "Print the DLL from the first to the last object."
  (princ "< " stream)
  (map-dll (lambda (obj)
             (princ obj stream)
             (princ #\SPACE stream))
           dll)
  (princ #\> stream))

(defun circular-dll-p (dll)
  "Is the DLL circular?"
  (labels ((recur (next)
             (let ((tail (dll-tail next)))
               (cond ((eql tail dll)
                      t)
                     (tail (recur tail))
                     (t nil)))))
    (recur dll)))

(defun map-dll (fn dll)
  "Call the function on each object in the DLL will start with the first DLL in order unless
 circular in which case will start with the DLL given."
  (let ((first (first-dll dll nil)))
    (labels ((map-tails (dll)
               (funcall fn (dll-obj dll))
               (let ((tail (dll-tail dll)))
                 (unless (eql tail first) ; circular
                   (when tail
                     (map-tails tail))))))
        (map-tails first)))
  dll)







(defun 13a ()
  (with-open-file (stream #P"~/common-lisp/advent-of-code/src/2022/input13.txt")
    (loop for expr1 = (read-expr stream nil :eof)
          for expr2 = (read-expr stream nil :eof)
          until (or (eq expr1 :eof) (eq expr2 :eof))
          for index from 1
          do (format t "Testing #~A: ~A ~A:   ~A~%" index expr1 expr2 (ordered expr1 expr2))
          when (ordered expr1 expr2)
            summing index)))

(defun ordered (expr1 expr2)
  (loop for ex1 in expr1
        for ex2 in expr2
        do (cond ((and (integerp ex1) (integerp ex2))
                  (cond ((< ex1 ex2)
                         (return-from ordered t))
                        ((> ex1 ex2)
                         (return-from ordered nil))))
                 ((and (listp ex1) (listp ex2))
                  (return-from ordered (ordered ex1 ex2)))
                 ((and (listp ex1) (integerp ex2))
                  (return-from ordered (ordered ex1 (list ex2))))
                 ((and (integerp ex1) ex2)
                  (return-from ordered (ordered (list ex1) ex2))))
        finally (return (<= (length expr1) (length expr2)))))

(defun read-expr (stream eof-error-p eof-value)
   (let ((line (read-line stream eof-error-p eof-value)))
     (cond
           ((eq line :eof)
            :eof)
           ((= 0 (length line))
            (read-expr stream eof-error-p eof-value))
           (t (progn (replace-all #\, #\SPACE line)
                     (replace-all #\[ #\( line)
                     (replace-all #\] #\) line)
                     (read-from-string line eof-error-p eof-value))))))
 
(defun replace-all (from-char to-char string)
  (loop for index from 0
        for char across string
        when (char= char from-char)
          do (setf (char string index) to-char)))



(defun lispify (syms)
  (labels ((walk ()
             (loop with result
                   for sym = (pop syms)
                   while syms
                   until (eq sym '|\)|)
                   if (eq sym '|\(|)
                     do (push (walk) result)
                   else do (push sym result)
                   finally (return (print (nreverse result))))))
   (walk)))

(defun lispify-string (string)
  (let ((pointer 0))
    (labels ((peek ()
               (case (char string pointer)
                 (#\( (incf pointer)
                      :paran)
                 (#\) (incf pointer)
                      :end-paran)
                 (#\SPACE (incf pointer)
                          :delim)
                 (otherwise
                  (loop for index from pointer below (length string)
                        until (case (char string index)
                                (#\( t)
                                (#\) t)
                                (#\SPACE t))
                        finally (let ((sym (intern (subseq string pointer index))))
                                  (setf pointer index)
                                  (return sym))))))
             (walk ()
               (loop with result = '()
                     while (< pointer (1- (length string)))
                     for next = (peek)
                     until (eq next :end-paran)
                     if (eq next :paran)
                       do (push (walk) result)
                     else if (not (eq next :delim))
                            do (push next result)
                     finally (return (nreverse result)))))
      (walk))))



(defun 21a ()
 (let ((monkeys (make-hash-table :test #'equalp)))
   (labels ((parse-line (line)
              (let* ((monkey (subseq line 0 4))
                     (rest (subseq line 6))
                     (maybe-num (every #'digit-char-p rest)))
                (setf (gethash monkey monkeys)
                      (if maybe-num
                         (let ((num (parse-integer rest)))
                           num)
                         (let ((m1 (subseq rest 0 4))
                               (op (read-from-string (subseq rest 5 6)))
                               (m2 (subseq rest 7)))
                           `(,op (gethash ,m1 monkeys)
                                 (gethash ,m2 monkeys))))))))
     (with-file-per-line #P"~/common-lisp/advent-of-code/src/2022/test21.txt"
       (lambda (line)
         (parse-line (nstring-upcase line)))))
   (gethash "root" monkeys)))
