(defpackage #:aoc.2021
  (:use #:cl)
  (:nicknames #:aoc #:advent-of-code))

(defpackage #:com.mtreis86.aoc.2020
  (:use #:cl)
  (:nicknames #:aoc.2020 #:advent-of-code-2020))
