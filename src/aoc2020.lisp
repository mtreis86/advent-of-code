#|

Read in from a file, when two entries add up to a number, print the multiplicant of them.

Two ways to approach this:
  For sparse inputs read into a list and search for complement.
  For dense inputs read into a vector and check for complement.

|#

(in-package :com.mtreis86.aoc.2020)

(defmacro with-aoc-data-as-stream (filename stream &body body)
  `(with-open-file (,stream (pathname (concatenate 'string
                                                  "./common-lisp/advent-of-code/src/2020/"
                                                  ,filename)))
    ,@body))



(defun aoc1a ()
  (let ((vector (make-array 2020 :element-type 'bit :initial-element 0)))
    (with-aoc-data-as-stream "01.txt" stream
      (loop :for number = (read stream nil)
            while number
            do (when (zerop (aref vector number))
                 (setf (aref vector number) 1)
                 (let ((complement (- 2020 number)))
                   (when (plusp (aref vector complement))
                     (let ((result (* number complement)))
                       (format t "num1: ~A~%num2: ~A~%mult: ~A~%"
                               number complement result)
                       (return-from aoc1a result)))))))
    nil))

(defun aoc1b ()
  (with-aoc-data-as-stream "01.txt" stream
    (loop :for num1 = (read stream nil)
          while num1
          collecting num1 into num1-list
          do (loop with num2-list = (remove num1 num1-list)
                   :for num2 in num2-list
                   do (loop with num3-list = (remove num2 num2-list)
                            :for num3 in num3-list
                            do (when (= (+ num1 num2 num3) 2020)
                                 (return-from aoc1b (* num1 num2 num3)))))))
  nil)


#|
--- Day 2: Password Philosophy ---

Your flight departs in a few days from the coastal airport; the easiest way down to the coast from
here is via toboggan.

The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day. "Something's wrong with
our computers; we can't log in!" You ask if you can take a look.

Their password database seems to be a little corrupted: some of the passwords wouldn't have been
allowed by the Official Toboggan Corporate Policy that was in effect when they were chosen.

To try to debug the problem, they have created a list (your puzzle input) of passwords (according)
to the corrupted database and the corporate policy when that password was set.

For example, suppose you have the following list:

1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc

Each line gives the password policy and then the password. The password policy indicates the lowest
and highest number of times a given letter must appear for the password to be valid. For example,
1-3 a means that the password must contain a at least 1 time and at most 3 times.

In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no
instances of b, but needs at least 1. The first and third passwords are valid: they contain one a
or nine c, both within the limits of their respective policies.

How many passwords are valid according to their policies?

|#

(defun aoc2a ()
  (with-aoc-data-as-stream "02.txt" stream
    (loop :for line = (read-line stream nil)
          while line
          do (let ((low (split-low line #\-))
                   (high (split-low (split-high line #\-) #\SPACE))
                   (letter (split-low (split-high line #\SPACE) #\)))
                   (password (split-high (split-high line #\SPACE) #\SPACE))
                   (char-list '()))
               (format t "low: ~A high: ~A letter: ~A pass: ~A" low high letter password)
               (loop :for char across password
                     do (if (assoc char char-list)
                            (incf (cdr (assoc char char-list)))
                            (push (cons char 1) char-list)))
               (print char-list))))) 


(defun split-low (sequence split-char)
  (first (split-first sequence split-char)))

(defun split-high (sequence split-char)
  (rest (split-first sequence split-char)))

(defun split-first (sequence split-char)
  "Split the sequence into the head and tail with the first instance of the split-char being the
  division point. Returns them as multiple values in order."
  (loop :for char :across sequence
        :for char-index :from 0
        :when (char= char split-char) :do
          (return-from split-first (cons (subseq sequence 0 char-index)
                                         (subseq sequence (1+ char-index))))))

(defun split (sequence split-char)
  "Split the sequence into a list of strings using the split-char as the divisions."
  (loop :for char :across sequence
        :for char-index :from 0
        :with last-split = 0
        :with result = nil
        :when (char= char split-char) :do
          (push (subseq sequence last-split char-index) result)
          (setf last-split (1+ char-index))
        :finally (return (append (nreverse result) (list (subseq sequence last-split))))))





#|

--- Day 3: Toboggan Trajectory ---

With the toboggan login problems resolved, you set off toward the airport.
While travel by toboggan might be easy, it's certainly not safe: there's very
minimal steering and the area is covered in trees. You'll need to see which
angles will take you near the fewest trees.

Due to the local geology, trees in this area only grow on exact integer
coordinates in a grid. You make a map (your puzzle input) of the open
squares (.) and trees (#) you can see. For example:

..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#

These aren't the only trees, though; due to something you read about once
involving arboreal genetics and biome stability, the same pattern repeats
to the right many times:

..##.........##.........##.........##.........##.........##.......  --->
#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....  --->
.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........#.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...##....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#  --->

You start on the open square (.) in the top-left corner and need to reach
the bottom (below the bottom-most row on your map).

The toboggan can only follow a few specific slopes (you opted for a cheaper)
model that prefers rational numbers; start by counting all the trees you would encounter
for the slope right 3, down 1:

From your starting position at the top-left, check the position that is
right 3 and down 1. Then, check the position that is right 3 and down 1
from there, and so on until you go past the bottom of the map.

The locations you'd check in the above example are marked here with O where
there was an open square and X where there was a tree:

..##.........##.........##.........##.........##.........##.......  --->
#..O#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....X..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#O#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..X...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.X#.......#.##.......#.##.......#.##.......#.##.....  --->
.#.#.#....#.#.#.#.O..#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........X.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.X#...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...#X....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...X.#.#..#...#.#.#..#...#.#.#..#...#.#  --->

In this example, traversing the map using this slope would cause you to
'encounter 7 trees.

Starting at the top-left corner of your map and following a slope of
right 3 and down 1, how many trees would you encounter?


|#

(defun aoc3a ()
  (with-aoc-data-as-stream "03.txt" stream
    (let ((array (make-array '(323 31))))
      (loop :for line = (read-line stream nil)
            :for line-index :from 0 :below 323
            :do (loop :for char-index :from 0 :below 31
                     :do (setf (aref array line-index char-index)
                               (subseq line char-index (1+ char-index)))))
      array
      (loop :for down :from 0 :below 323
            :with count = 0
            :for right :from 0 :by 3
            :do (let ((right (mod right 31)))
                  (when (string= (aref array down right) #\#)
                    (incf count)))
            :finally (return count)))))
 
#|

--- Part Two ---

Time to check the rest of the slopes - you need to minimize the probability of a sudden arboreal
stop, after all.

Determine the number of trees you would encounter if, for each of the following slopes, you start
at the top-left corner and traverse the map all the way to the bottom:

    Right 1, down 1.
    Right 3, down 1. (This is the slope you already checked.)
    Right 5, down 1.
    Right 7, down 1.
    Right 1, down 2.

In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s) respectively; multiplied
together, these produce the answer 336.

What do you get if you multiply together the number of trees encountered on each of the listed
slopes?


|#


(defun aoc3b ()
  (with-aoc-data-as-stream "03.txt" stream
    (let ((array (make-array '(323 31))))
      (loop :for line = (read-line stream nil)
            :for line-index :from 0 :below 323
            :do (loop :for char-index :from 0 :below 31
                     :do (setf (aref array line-index char-index)
                               (subseq line char-index (1+ char-index)))))
      array
      (let ((ex1 0) (ex2 0) (ex3 0) (ex4 0) (ex5 0))
        (loop :for down :from 0 :below 323
              :for right :from 0
              :do (let ((right (mod right 31)))
                    (when (string= (aref array down right) #\#)
                      (incf ex1))))
        (loop :for down :from 0 :below 323
              :for right :from 0 :by 3
              :do (let ((right (mod right 31)))
                    (when (string= (aref array down right) #\#)
                      (incf ex2))))
        (loop :for down :from 0 :below 323
              :for right :from 0 :by 5
              :do (let ((right (mod right 31)))
                    (when (string= (aref array down right) #\#)
                      (incf ex3))))
        (loop :for down :from 0 :below 323
              :for right :from 0 :by 7
              :do (let ((right (mod right 31)))
                    (when (string= (aref array down right) #\#)
                      (incf ex4))))
        (loop :for down :from 0 :below 323 :by 2
              :for right :from 0
              :do (let ((right (mod right 31)))
                    (when (string= (aref array down right) #\#)
                      (incf ex5))))
        (* ex1 ex2 ex3 ex4 ex5)))))


#|
--- Day 4: Passport Processing ---

You arrive at the airport only to realize that you grabbed your North Pole Credentials instead of
your passport. While these documents are extremely similar, North Pole Credentials aren't issued
by a country and therefore aren't actually valid documentation for travel in most of the world.

It seems like you're not the only one having problems, though; a very long line has formed for the
automatic passport scanners, and the delay could upset your travel itinerary.

Due to some questionable network security, you realize you might be able to solve both of these
problems at the same time.

The automatic passport scanners are slow because they're having trouble detecting which passports
have all required fields. The expected fields are as follows:

    byr (Birth Year)
    iyr (Issue Year)
    eyr (Expiration Year)
    hgt (Height)
    hcl (Hair Color)
    ecl (Eye Color)
    pid (Passport ID)
    cid (Country ID)

Passport data is validated in batch files (your puzzle input). Each passport is represented as
a sequence of key:value pairs separated by spaces or newlines. Passports are separated
by blank lines.

Here is an example batch file containing four passports:

ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in

The first passport is valid - all eight fields are present. The second passport is invalid -
it is missing hgt (the Height field).

The third passport is interesting; the only missing field is cid, so it looks like data from
North Pole Credentials, not a passport at all! Surely, nobody would mind if you made the system
temporarily ignore missing cid fields. Treat this "passport" as valid.

The fourth passport is missing two fields, cid and byr. Missing cid is fine, but missing any other
field is not, so this passport is invalid.

According to the above rules, your improved system would report 2 valid passports.

Count the number of valid passports - those that have all required fields. Treat cid as optional.
In your batch file, how many passports are valid?


|#


(defun aoc4a ()
  (populate-id-table)
  ;; (get-id-table))
  (validate-id-table))

(let ((ids (make-hash-table :test 'eq))
      (id-count 0))
  (defun populate-id-table ()
    (setf ids (make-hash-table))
    (with-aoc-data-as-stream "04.txt" stream
      (loop :for line = (read-line stream nil)
            :with current-id
            :while line :do
              (unless current-id
                (setf current-id (make-instance 'id)))
              (if (string= line "")
                  (setf (gethash (id-number current-id) ids) current-id
                        current-id nil)
                  (update-all current-id line)))))
  (defun get-id-table ()
    ids)
  (defun validate-id-table ()
    (loop :with valid = 0
          :with invalid = 0
          :for id-number :being :the hash-key :of ids :do
            (let ((id (gethash id-number ids)))
              (if (valid-id-p id)
                  (incf valid)
                  (incf invalid)))
          :finally (format t "valid: ~A invalid: ~A~%" valid invalid)))
  (defclass id ()
    ((id-number :initform (incf id-count) :reader id-number)
     (birth-year :initform nil :accessor birth-year)
     (issue-year :initform nil :accessor issue-year)
     (expiration-year :initform nil :accessor expiration-year)
     (height :initform nil :accessor height)
     (hair-color :initform nil :accessor hair-color)
     (eye-color :initform nil :accessor eye-color)
     (passport-id  :initform nil :accessor passport-id)
     (country-id :initform nil :accessor country-id))))


(defun id= (id1 id2)
  (or (= (id-number id1) (id-number id2))))

(defun valid-id-p (id)
  (and
   (valid-year (birth-year id) 4 1920 2002)
   (valid-year (issue-year id) 4 2010 2020)
   (valid-year (expiration-year id) 4 2020 2030)
   (valid-height (height id))
   (valid-color-code (hair-color id))
   (valid-string (eye-color id) '("amb" "blu" "brn" "gry" "grn" "hzl" "oth"))
   (valid-pass-id (passport-id id)) 9))

(defun int-length (int)
  (ceiling (log int 10)))

(defun valid-pass-id (passport-id)
  (and passport-id
       (stringp passport-id)
       (= (length passport-id) 9)))

(defun valid-year (year length min max)
  (and year
       (integerp year)
       (= (int-length year) length)
       (>= year min)
       (<= year max)))

(defun valid-string (color color-list)
  (and color
       (find color color-list :test #'string=)))
  
(defun valid-height (height)
  (and height
       (let ((cm (split-over height "cm"))
             (in (split-over height "in")))
         (and (or cm in)
              (if cm
                  (and (>= 150) (<= 193))
                  (and (>= 59) (<= 76)))))))
                  
(defun split-over (sequence split-string))


(defun first-char (string)
  (print string)
  (subseq string 0 1))

(defun rest-chars (string &optional (end nil))
  (print string)
  (subseq string 1 end))

(defun valid-color-code (color)
  (and color))







(defun update-all (id tag-string)
  (loop :for next-tag :in (split tag-string #\SPACE) :do
    (let* ((tag (split next-tag #\:))
           (key (first tag))
           (val (first (rest tag))))
      (update-id id key val)))
  id)

(defun update-id (id tag value)
  (case (intern (string-upcase tag))
    (byr (setf (birth-year id) (parse-integer value)))
    (iyr (setf (issue-year id) (parse-integer value)))
    (eyr (setf (expiration-year id) (parse-integer value)))
    (hgt (setf (height id) value))
    (hcl (setf (hair-color id) value))
    (ecl (setf (eye-color id) value))
    (pid (setf (passport-id id) value)) ; TODO one entry has 153cm
    (cid (setf (country-id id) (parse-integer value))))
  id)


#|
--- Part Two ---

The line is moving more quickly now, but you overhear airport security talking about how passports
with invalid data are getting through. Better add some data validation, quick!

You can continue to ignore the cid field, but each other field has strict rules about what values
are valid for automatic validation:

    byr (Birth Year) - four digits; at least 1920 and at most 2002.
    iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    hgt (Height) - a number followed by either cm or in:
        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.
    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID) - a nine-digit number, including leading zeroes.
    cid (Country ID) - ignored, missing or not.

Your job is to count the passports where all required fields are both present and valid according
to the above rules. Here are some example values:

byr valid:   2002
byr invalid: 2003

hgt valid:   60in
hgt valid:   190cm
hgt invalid: 190in
hgt invalid: 190

hcl valid:   #123abc
hcl invalid: #123abz
hcl invalid: 123abc

ecl valid:   brn
ecl invalid: wat

pid valid:   000000001
pid invalid: 0123456789

Here are some invalid passports:

eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007

Here are some valid passports:

pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719

Count the number of valid passports - those that have all required fields and valid values.
Continue to treat cid as optional. In your batch file, how many passports are valid?
|#



















#|
--- Day 5: Binary Boarding ---

You board your plane only to discover a new problem: you dropped your boarding pass! You aren't
sure which seat is yours, and all of the flight attendants are busy with the flood of people
that suddenly made it through passport control.

You write a quick program to use your phone's camera to scan all of the nearby boarding passes
(your puzzle input); perhaps you can find your seat through process of elimination.

Instead of zones or groups, this airline uses binary space partitioning to seat people. A seat
might be specified like FBFBBFFRLR, where F means "front", B means "back", L means "left",
and R means "right".

The first 7 characters will either be F or B; these specify exactly one of the 128 rows on the
plane (numbered 0 through 127). Each letter tells you which half of a region the given seat is in.
Start with the whole list of rows; the first letter indicates whether the seat is in the
front (0 through 63) or the back (64 through 127). The next letter indicates which half of that
region the seat is in, and so on until you're left with exactly one row.

For example, consider just the first seven characters of FBFBBFFRLR:

    Start by considering the whole range, rows 0 through 127.
    F means to take the lower half, keeping rows 0 through 63.
    B means to take the upper half, keeping rows 32 through 63.
    F means to take the lower half, keeping rows 32 through 47.
    B means to take the upper half, keeping rows 40 through 47.
    B keeps rows 44 through 47.
    F keeps rows 44 through 45.
    The final F keeps the lower of the two, row 44.

The last three characters will be either L or R; these specify exactly one of the 8 columns of
seats on the plane (numbered 0 through 7). The same process as above proceeds again, this time
with only three steps. L means to keep the lower half, while R means to keep the upper half.

For example, consider just the last 3 characters of FBFBBFFRLR:

    Start by considering the whole range, columns 0 through 7.
    R means to take the upper half, keeping columns 4 through 7.
    L means to take the lower half, keeping columns 4 through 5.
    The final R keeps the upper of the two, column 5.

So, decoding FBFBBFFRLR reveals that it is the seat at row 44, column 5.

Every seat also has a unique seat ID: multiply the row by 8, then add the column. In this example,
the seat has ID 44 * 8 + 5 = 357.

Here are some other boarding passes:

    BFFFBBFRRR: row 70, column 7, seat ID 567.
    FFFBBBFRRR: row 14, column 7, seat ID 119.
    BBFFBBFRLL: row 102, column 4, seat ID 820.

As a sanity check, look through your list of boarding passes. What is the highest seat ID on
a boarding pass?

|#


