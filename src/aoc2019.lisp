;;;; Advent of code 2019-2020 by Michael Reis

(in-package #:aoc)

(deftest run-all-tests ()
  (combine-results
    (test-one)))

;;; Day One

#|--- Day 1: The Tyranny of the Rocket Equation ---

Santa has become stranded at the edge of the Solar System while delivering presents to other
planets! To accurately calculate his position in space, safely align his warp drive, and return to
Earth in time to save Christmas, he needs you to bring him measurements from fifty stars.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent
calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star.
Good luck!

The Elves quickly load you into a spacecraft and prepare to launch.

At the first Go / No Go poll, every Elf is Go until the Fuel Counter-Upper. They haven't
determined the amount of fuel required yet.

Fuel required to launch a given module is based on its mass. Specifically, to find the fuel
required for a module, take its mass, divide by three, round down, and subtract 2.

For example:

    For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
    For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required
    is also 2.
    For a mass of 1969, the fuel required is 654.
    For a mass of 100756, the fuel required is 33583.

The Fuel Counter-Upper needs to know the total fuel requirement. To find it, individually
calculate the fuel needed for the mass of each module (your puzzle input), then add together
all the fuel values.

What is the sum of the fuel requirements for all of the modules on your spacecraft?
|#

(deftest test-one ()
    (check (eq 2 (mass->fuel 12))
           (eq 2 (mass->fuel 14))
           (eq 654 (mass->fuel 1969))
           (eq 33583 (mass->fuel 100756))))

(defun mass->fuel (mass)
  "Calculate fuel requirement based on f(x) = (round-down (x / 3)) - 2"
  (- (floor mass 3) 2))

(defun aoc2019-one ()
  (with-open-file (input #P"./common-lisp/advent-of-code/src/2019/one.txt")
       (loop for line = (read input nil)
          while line
          summing (mass->fuel line))))

#|--- Part Two ---

During the second Go / No Go poll, the Elf in charge of the Rocket Equation Double-Checker stops the
launch sequence. Apparently, you forgot to include additional fuel for the fuel you just added.

Fuel itself requires fuel just like a module - take its mass, divide by three, round down, and
subtract 2. However, that fuel also requires fuel, and that fuel requires fuel, and so on. Any mass
that would require negative fuel should instead be treated as if it requires zero fuel; the
remaining mass, if any, is instead handled by wishing really hard, which has no mass and is outside
the scope of this calculation.

So, for each module mass, calculate its fuel and add it to the total. Then, treat the fuel amount
you just calculated as the input mass and repeat the process, continuing until a fuel requirement
is zero or negative. For example:

    A module of mass 14 requires 2 fuel. This fuel requires no further fuel (2 divided by 3 and)
rounded down is 0, which would call for a negative fuel, so the total fuel required is still just 2.
    At first, a module of mass 1969 requires 654 fuel. Then, this fuel requires 216 more fuel
(654 / 3 - 2). 216 then requires 70 more fuel, which requires 21 fuel, which requires 5 fuel,
which requires no further fuel. So, the total fuel required for a module of mass 1969 is
654 + 216 + 70 + 21 + 5 = 966.
    The fuel required by a module of mass 100756 and its fuel is:
33583 + 11192 + 3728 + 1240 + 411 + 135 + 43 + 12 + 2 = 50346.

What is the sum of the fuel requirements for all of the modules on your spacecraft when also taking
into account the mass of the added fuel? (Calculate the fuel requirements for each module)
separately, then add them all up at the end.
|#

(deftest test-one-pt2 ()
  (check (eq 2 (mass->fuel2 14))
         (eq 966 (mass->fuel2 1969))
         (eq 50346 (mass->fuel2 100756))))

(defun mass->fuel2 (mass)
  "Calculate fuel requirements considering the fuel itself needs to be flown as well."
    (let ((fuel-for-load (max 0 (- (floor mass 3) 2))))
      (if (> fuel-for-load 5)
          (+ fuel-for-load (mass->fuel2 fuel-for-load))
          fuel-for-load)))

(defun aoc2019-one-pt2 ()
  (with-file (input "one.txt")
      (loop for line = (read input nil)
          while line
          summing (mass->fuel2 line))))


#|--- Day 2: 1202 Program Alarm ---

On the way to your gravity assist around the Moon, your ship computer beeps angrily about a
"1202 program alarm". On the radio, an Elf is already explaining how to handle the situation:
"Don't worry, that's perfectly norma--" The ship computer bursts into flames.

You notify the Elves that the computer's magic smoke seems to have escaped. "That computer ran
Intcode programs like the gravity assist program it was working on; surely there are enough spare
parts up there to build a new Intcode computer!"

An Intcode program is a list of integers separated by commas (like 1,0,0,3,99). To run one, start
by looking at the first integer (called position 0). Here, you will find an opcode -
either 1, 2, or 99. The opcode indicates what to do; for example, 99 means that the program is
finished and should immediately halt. Encountering an unknown opcode means something went wrong.

Opcode 1 adds together numbers read from two positions and stores the result in a third position.
The three integers immediately after the opcode tell you these three positions - the first two
indicate the positions from which you should read the input values, and the third indicates the
position at which the output should be stored.

For example, if your Intcode computer encounters 1,10,20,30, it should read the values at
positions 10 and 20, add those values, and then overwrite the value at position 30 with their sum.

Opcode 2 works exactly like opcode 1, except it multiplies the two inputs instead of adding them.
Again, the three integers after the opcode indicate where the inputs and outputs are,
not their values.

Once you're done processing an opcode, move to the next one by stepping forward 4 positions.

For example, suppose you have the following program:

1,9,10,3,2,3,11,0,99,30,40,50

For the purposes of illustration, here is the same program split into multiple lines:

1,9,10,3,
2,3,11,0,
99,
30,40,50

The first four integers, 1,9,10,3, are at positions 0, 1, 2, and 3. Together, they represent the
first opcode (1, addition), the positions of the two inputs (9 and 10), and the position of the
output (3). To handle this opcode, you first need to get the values at the input positions:
position 9 contains 30, and position 10 contains 40. Add these numbers together to get 70.
Then, store this value at the output position; here, the output position (3) is at position 3,
so it overwrites itself. Afterward, the program looks like this:

1,9,10,70,
2,3,11,0,
99,
30,40,50

Step forward 4 positions to reach the next opcode, 2. This opcode works just like the previous,
but it multiplies instead of adding. The inputs are at positions 3 and 11; these positions contain
70 and 50 respectively. Multiplying these produces 3500; this is stored at position 0:

3500,9,10,70,
2,3,11,0,
99,
30,40,50

Stepping forward 4 more positions arrives at opcode 99, halting the program.

Here are the initial and final states of a few more small programs:

    1,0,0,0,99 becomes 2,0,0,0,99 (1 + 1 = 2).
    2,3,0,3,99 becomes 2,3,0,6,99 (3 * 2 = 6).
    2,4,4,5,99,0 becomes 2,4,4,5,99,9801 (99 * 99 = 9801).
    1,1,1,4,99,5,6,0,99 becomes 30,1,1,4,2,5,6,0,99.

Once you have a working computer, the first step is to restore the gravity assist program
(your puzzle input) to the "1202 program alarm" state it had just before the last computer caught
fire. To do this, before running the program, replace position 1 with the value 12 and replace
position 2 with the value 2. What value is left at position 0 after the program halts?
|#
(defun opcodep (number)
  "Returns true if the number is an opcode, 1 2 or 99."
  (or (= number 1)
      (= number 2)
      (= number 99)))

(deftype opcode ()
  "An opcode is a number, either 1, 2, or 99 that represents program instructions."
  `(satisfies opcodep))

(defun dispatch (program index)
  "Process opcode at the index and destructively modify the program as specified by the opcode and
the following pointers."
  (let* ((opcode (svref program index))
         (position1 (svref program (+ index 1)))
         (position2 (svref program (+ index 2)))
         (position-result (svref program (+ index 3)))
         (contents1 (svref program position1))
         (contents2 (svref program position2)))
    ;; (format t "~%~%~A ~A ~A ~A" opcode position1 position2 position-result)
    (setf (svref program position-result)
          (cond
            ((= opcode 1) (+ contents1 contents2))
            ((= opcode 2) (* contents1 contents2))))))

(defun run-intercode-program (program)
  "Loop across the vector representing the program, breaking it up into chunks of four, until
running into a quitting opcode. Ensures opcodes are legal."
  (loop for index from 0 below (array-dimension program 0) by 4
        for opcode = (svref program index)
        until (or (not (typep opcode 'opcode)) ; silently quits when error
                  (= opcode 99))
        do (dispatch program index))
  program)

(deftest test-two ()
  (check (equalp (run-intercode-program #(1 9 10 3 2 3 11 0 99 30 40 50))
                 #(3500 9 10 70 2 3 11 0 99 30 40 50))
         (equalp (run-intercode-program #(1 0 0 0 99))
                 #(2 0 0 0 99))
         (equalp (run-intercode-program #(2 3 0 3 99))
                 #(2 3 0 6 99))
         (equalp (run-intercode-program #(2 4 4 5 99 0))
                 #(2 4 4 5 99 9801))
         (equalp (run-intercode-program #(1 1 1 4 99 5 6 0 99))
                 #(30 1 1 4 2 5 6 0 99))))

(defun aoc2019-two ()
  "Replace position 1 with the value 12 and replace position 2 with the value 2.
  What value is left at position 0 after the program halts?"
  (with-csv-as-vector (program "two.txt")
    (setf (svref program 1) 12
          (svref program 2) 2)
    (svref (run-intercode-program program) 0)))


#|--- Part Two ---

"Good, the new computer seems to be working correctly! Keep it nearby during this mission - you'll
probably use it again. Real Intcode computers support many more features than your new one, but
we'll let you know what they are as you need them."

"However, your current priority should be to complete your gravity assist around the Moon. For
this mission to succeed, we should settle on some terminology for the parts you've already built."

Intcode programs are given as a list of integers; these values are used as the initial state for
the computer's memory. When you run an Intcode program, make sure to start by initializing memory
to the program's values. A position in memory is called an address (for example, the first value)
in memory is at "address 0".

Opcodes (like 1, 2, or 99) mark the beginning of an instruction. The values used immediately after
an opcode, if any, are called the instruction's parameters. For example, in the instruction
1,2,3,4, 1 is the opcode; 2, 3, and 4 are the parameters. The instruction 99 contains only an
opcode and has no parameters.

The address of the current instruction is called the instruction pointer; it starts at 0. After
an instruction finishes, the instruction pointer increases by the number of values in the
instruction; until you add more instructions to the computer, this is always 4
(1 opcode + 3 parameters) for the add and multiply instructions. (The halt instruction would)
increase the instruction pointer by 1, but it halts the program instead.

"With terminology out of the way, we're ready to proceed. To complete the gravity assist, you need
to determine what pair of inputs produces the output 19690720."

The inputs should still be provided to the program by replacing the values at addresses 1 and 2,
just like before. In this program, the value placed in address 1 is called the noun, and the value
placed in address 2 is called the verb. Each of the two input values will be between 0 and 99,
inclusive.

Once the program has halted, its output is available at address 0, also just like before. Each
time you try a pair of inputs, make sure you first reset the computer's memory to the values in
the program (your puzzle input) - in other words, don't reuse memory from a previous attempt.

Find the input noun and verb that cause the program to produce the output 19690720. What is 100
* noun + verb? (For example, if noun=12 and verb=2, the answer would be 1202.)
|#

(defun aoc2019-two-pt2 ()
  (loop for noun from 0 to 99
        until (loop for verb from 0 to 99
                    until (= 19690720
                             (with-csv-as-vector (program "two.txt")
                               (setf (svref program 1) noun
                                     (svref program 2) verb)
                               (svref (run-intercode-program program) 0)))
                    finally (values noun verb))))

#|
--- Day 3: Crossed Wires ---

The gravity assist was successful, and you're well on your way to the Venus refuelling station.
During the rush back on Earth, the fuel management system wasn't completely installed, so that's
next on the priority list.

Opening the front panel reveals a jumble of wires. Specifically, two wires are connected to a
central port and extend outward on a grid. You trace the path each wire takes as it leaves the
central port, one wire per line of text (your puzzle input).

The wires twist and turn, but the two wires occasionally cross paths. To fix the circuit, you
need to find the intersection point closest to the central port. Because the wires are on a grid,
use the Manhattan distance for this measurement. While the wires do technically cross right at
the central port where they both start, this point does not count, nor does a wire count as
crossing with itself.

For example, if the first wire's path is R8,U5,L5,D3, then starting from the central port (o),
it goes right 8, up 5, left 5, and finally down 3:

...........
...........
...........
....+----+.
....|....|.
....|....|.
....|....|.
.........|.
.o-------+.
...........

Then, if the second wire's path is U7,R6,D4,L4, it goes up 7, right 6, down 4, and left 4:

...........
.+-----+...
.|.....|...
.|..+--X-+.
.|..|..|.|.
.|.-X--+.|.
.|..|....|.
.|.......|.
.o-------+.
...........

These wires cross at two locations (marked X), but the lower-left one is closer to the central
port: its distance is 3 + 3 = 6.

Here are a few more examples:

    R75,D30,R83,U83,L12,D49,R71,U7,L72
    U62,R66,U55,R34,D71,R55,D58,R83 = distance 159
    R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
    U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = distance 135

What is the Manhattan distance from the central port to the closest intersection?
|#

(defun import-wire-instruction (instruction-string)
  "Convert instruction string into lispy wire directions, eg \"R75\" -> ('right 75)"
  (list (dir-char-to-symb (subseq instruction-string 0 1))
        (parse-integer (subseq instruction-string 1))))

(defun dir-char-to-symb (char)
  "Convert directional characters to symbol. eg r -> 'right"
  (if (typep char 'character)
      (return-from dir-char-to-symb
        (case char
          ((r R) 'right)
          ((l L) 'left)
          ((u U) 'up)
          ((d D) 'down)))
      (cond ((string= char "r") 'right)
            ((string= char "R") 'right)
            ((string= char "l") 'left)
            ((string= char "L") 'left)
            ((string= char "u") 'up)
            ((string= char "U") 'up)
            ((string= char "d") 'down)
            ((string= char "D") 'down)
            (t (error "Char was not one of the directional characters.")))))

(defun import-wire (wire-string &optional list)
  "Convert the wire-string into a list of lispy wire instructions."
  (let* ((next-comma (next-comma wire-string))
         (direction (dir-char-to-symb (subseq wire-string 0 1)))
         (distance (parse-integer (subseq wire-string 1 next-comma)))
         (dir-dist (list (cons direction distance))))
    (if next-comma
        (import-wire (subseq wire-string (+ next-comma 1))
                     (append list dir-dist))
        (append list dir-dist))))

(defun make-board (size)
  (assert (oddp size))
  (let ((offset (ceiling size 2)))
    (make-offset-2d-array size offset offset)))

(defun set-wire (board x y color)
  "Set the wire located at x,y to the color."
  (declare (optimize debug))
  (let ((current-wires (get-wire board x y)))
    (unless (member color current-wires)
      (setf (oaref board x y) (append (list color) current-wires)))
    board))

(defun get-wire (board x y)
  "Get the colors of the wires crossing through x,y in the board."
  (oaref board x y))

(defun shortest-distance (wire-strings)
  (let ((board (make-board 19))
        (wires (loop for this-wire in wire-strings collecting (import-wire this-wire)))
        (colors '(white black red))) ; TODO add colors
    (loop for wire in wires
          for color in colors
       do (loop with x = 0 with y = 0
                for (direction . distance) in wire
             do (loop for step from 0 below distance
                   do (set-wire board x y color)
                     (case direction
                       (up (incf y))
                       (right (incf x))
                       (down (decf y))
                       (left (decf x))))))
    (print-board board)))

(defun print-board (board)
  (loop for idx from 0 below (array-dimension board 1)
     do (loop for idy from 0 below (array-dimension board 0)
           do (format t "~A " (let ((count (length (aref board idx idy))))
                                (if (zerop count)
                                    '-
                                    count))))))


(deftest test-three ()
         (check (equal (import-wire "R8,U5,L5,D3")
                       '((right . 8) (up . 5) (left . 5) (down . 3)))
                (equal (import-wire "U7,R6,D4,L4")
                       '((up . 7) (right . 6) (down . 4) (left . 4)))
                (= 6 (shortest-distance '("R8,U5,L5,D3" "U7,R6,D4,L4")))
                (= 159 (shortest-distance '("R75,D30,R83,U83,L12,D49,R71,U7,L72"
                                            "U62,R66,U55,R34,D71,R55,D58,R83")))
                (= 135 (shortest-distance '("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
                                            "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")))))


(defun next-comma (string &optional (start 0))
  "Return the index of the next comma in the string starting at start. Returns nil if none found."
  (loop for char across (subseq string start)
       for index from 0
       when (char= #\, char) do (return-from next-comma (- index start))
       finally (return-from next-comma nil)))     

(defstruct (offset-2d-array (:constructor %make-offset-2d-array))
  array offset-x offset-y)

(defun make-offset-2d-array (size offset-x offset-y &key (initial-element nil))
  "An offset array is not zero indexed, having a 0,0 within the array, specified by the offset. A
  5x5 array with the 0 0 point being in the center would then have a 2 2 offset. Once created the
  functions to set and get the contents of the cell will be referencing the 0 0. Offset arrays will
  then be accessed using both positive and negative coordinates."
  (let ((offset-array (%make-offset-2d-array)))
    (setf (offset-2d-array-array offset-array)    (make-array (list size size)
                                                              :initial-element initial-element)
          (offset-2d-array-offset-x offset-array) offset-x
          (offset-2d-array-offset-y offset-array) offset-y)
    offset-array))

(defun oaref (array x y)
  "Return the contents of the offset 2d array at x y."
  (aref (offset-2d-array-array array)
        (- (offset-2d-array-offset-x array) x)
        (- (offset-2d-array-offset-y array) y)))

(defun (setf oaref) (args array x y)
  "Set the cell of the 2d offset array specified by x y to args."
    (setf (aref (offset-2d-array-array array) x y) args))

(defun manhattan-distance (pt1 pt2)
  "Return the manhattan-distance between the points in whatever dimension space they are in. The
  co-ordinates are written as a list, pt1 and pt2, '(x y) or '(x y z) for instance. If one list is
  longer than the other the trailing points are ignored."
  (loop for dim1 in pt1
     for dim2 in pt2
     while (and dim1 dim2)
       summing (abs (- dim1 dim2))))

(defun read-csv-to-vector (path)
  "Read in the CSV file, using commas as delimiters, returning it as a vector."
  (let ((csv-readtable (copy-readtable))
        (input-vector (make-array 0 :adjustable t :fill-pointer 0)))
    (set-syntax-from-char #\, #\Space csv-readtable)
    (let ((*readtable* csv-readtable))
      (with-open-file (input path)
        (loop for entry = (read input nil nil)
           while entry do
             (vector-push-extend entry input-vector 1))))
    (coerce input-vector 'simple-vector)))

(let ((working-directory #P"./common-lisp/advent-of-code/src/2019/"))
  (defmacro with-file ((stream filename) &body body)
    "Safely open a file in the lexically bound working-directory."
    `(with-open-file (,stream (concat ,working-directory ,filename))
       ,@body))
  (defmacro with-csv-as-vector ((stream filename) &body body)
    "Safely open a csv, specified by the filename within the lexically bound working directory,
    as a vector and exectute the body."
    `(let ((,stream (read-csv-to-vector (concat ,working-directory ,filename))))
       ,@body)))

(defgeneric concat (seq1 seq2)
  (:documentation "Attempt to concatenate seq1 and seq2. If a return-type is specified, use that otherwise
use seq1's type."))
(defmethod concat ((dir pathname) (file string))
  (pathname (concatenate 'string (namestring dir) file)))
