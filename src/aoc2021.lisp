(in-package :aoc.2021)

#|--- Day 1: Sonar Sweep ---

You're minding your own business on a ship at sea when the overboard alarm goes off! You rush to
see if you can help. Apparently, one of the Elves tripped and accidentally sent the sleigh keys
flying into the ocean!

Before you know it, you're inside a submarine the Elves keep ready for situations like this.
It's covered in Christmas lights (because of course it is), and it even has an experimental
antenna that should be able to track the keys if you can boost its signal strength high enough;
there's a little meter that indicates the antenna's signal strength by displaying 0-50 stars.

Your instincts tell you that in order to save Christmas, you'll need to get all fifty stars
by December 25th.

Collect stars by solving puzzles. Two puzzles will be made available on each day in the
Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle
grants one star. Good luck!

As the submarine drops below the surface of the ocean, it automatically performs a sonar
sweep of the nearby sea floor. On a small screen, the sonar sweep report (your puzzle input)
appears: each line is a measurement of the sea floor depth as the sweep looks further and
further away from the submarine.

For example, suppose you had the following report:

199
200
208
210
200
207
240
269
260
263

This report indicates that, scanning outward from the submarine, the sonar sweep found depths of
199, 200, 208, 210, and so on.

The first order of business is to figure out how quickly the depth increases, just so you know what
you're dealing with - you never know if the keys will get carried into deeper water by an
ocean current or a fish or something.

To do this, count the number of times a depth measurement increases from the previous measurement.
(There is no measurement before the first measurement.) In the example above,
the changes are as follows:

199 (N/A - no previous measurement)
200 (increased)
208 (increased)
210 (increased)
200 (decreased)
207 (increased)
240 (increased)
269 (increased)
260 (decreased)
263 (increased)

In this example, there are 7 measurements that are larger than the previous measurement.

How many measurements are larger than the previous measurement? |#

(defun aoc-day-1 ()
  (let ((test #P"~/common-lisp/advent-of-code/src/2021/01a.txt")
        (full #P"~/common-lisp/advent-of-code/src/2021/01.txt"))
    (format t "Day 1:~% Part 1 test: ~A~% Part 1 full: ~A~% Part 2 test: ~A~% Part 2 full: ~A~%"
            (aoc-day-1-part-1 test) (aoc-day-1-part-1 full)
            (aoc-day-1-part-2 test) (aoc-day-1-part-2 full))))

(defun aoc-day-1-part-1 (path)
  (with-open-file (stream path)
    (loop with previous = (read stream nil)
          for number = (read stream nil)
          while number
          counting (> number previous) into count
          do (setf previous number)
          finally (return count))))


#|--- Part Two ---

Considering every single measurement isn't as useful as you expected:
there's just too much noise in the data.

Instead, consider sums of a three-measurement sliding window. Again considering the above example:

199  A      
200  A B    
208  A B C  
210    B C D
200  E   C D
207  E F   D
240  E F G  
269    F G H
260      G H
263        H

Start by comparing the first and second three-measurement windows. The measurements in the
first window are marked A (199, 200, 208); their sum is 199 + 200 + 208 = 607. The second window
is marked B (200, 208, 210); its sum is 618. The sum of measurements in the second window is
larger than the sum of the first, so this first comparison increased.

Your goal now is to count the number of times the sum of measurements in this sliding window
increases from the previous sum. So, compare A with B, then compare B with C, then C with D,
and so on. Stop when there aren't enough measurements left to create a new three-measurement sum.

In the above example, the sum of each three-measurement window is as follows:

A: 607 (N/A - no previous sum)
B: 618 (increased)
C: 618 (no change)
D: 617 (decreased)
E: 647 (increased)
F: 716 (increased)
G: 769 (increased)
H: 792 (increased)

In this example, there are 5 sums that are larger than the previous sum.

Consider sums of a three-measurement sliding window. How many sums are larger than the previous sum?
|#

(defun aoc-day-1-part-2 (path)
  (with-open-file (stream path)
    (loop with first = (read stream nil)
          with second = (read stream nil)
          with third = (read stream nil)
          for fourth = (read stream nil)
          while fourth
          counting (> (+ second third fourth) (+ first second third)) into count
          do (setf first second
                   second third
                   third fourth)
          finally (return count))))


#|--- Day 2: Dive! ---

Now, you need to figure out how to pilot this thing.
It seems like the submarine can take a series of commands like forward 1, down 2, or up 3:

forward X increases the horizontal position by X units.
down X increases the depth by X units.
up X decreases the depth by X units.

Note that since you're on a submarine, down and up affect your depth, and so they have the
opposite result of what you might expect.
The submarine seems to already have a planned course (your puzzle input). You should probably
figure out where it's going. For example:

forward 5
down 5
forward 8
up 3
down 8
forward 2

Your horizontal position and depth both start at 0. The steps above would then modify them
as follows:

forward 5 adds 5 to your horizontal position, a total of 5.
down 5 adds 5 to your depth, resulting in a value of 5.
forward 8 adds 8 to your horizontal position, a total of 13.
up 3 decreases your depth by 3, resulting in a value of 2.
down 8 adds 8 to your depth, resulting in a value of 10.
forward 2 adds 2 to your horizontal position, a total of 15.

After following these instructions, you would have a horizontal position of 15 and a depth
of 10. (Multiplying these together produces 150.)
Calculate the horizontal position and depth you would have after following the planned course.
What do you get if you multiply your final horizontal position by your final depth? |#

(defun aoc-day-2 ()
  (let ((test #P"~/common-lisp/advent-of-code/src/2021/02a.txt")
        (full #P"~/common-lisp/advent-of-code/src/2021/02.txt"))
    (format t "Day 2:~% Part 1 test: ~A~% Part 1 full: ~A~% Part 2 test: ~A~% Part 2 full: ~A~%"
            (aoc-day-2-part-1 test) (aoc-day-2-part-1 full)
            (aoc-day-2-part-2 test) (aoc-day-2-part-2 full))))


(defun aoc-day-2-part-1 (path)
  (with-open-file (stream path)
    (loop with depth = 0
          with horiz = 0
          for direction = (read stream nil)
          for distance = (read stream nil)
          while direction
          if (string= direction "DOWN")
            do (incf depth distance)
          else if (string= direction "UP")
                 do (decf depth distance)
          else if (string= direction "FORWARD")
                 do (incf horiz distance)
          finally (return (* depth horiz)))))


#|--- Part Two ---

Based on your calculations, the planned course doesn't seem to make any sense.
You find the submarine manual and discover that the process is actually slightly more complicated.

In addition to horizontal position and depth, you'll also need to track a third value, aim,
which also starts at 0. The commands also mean something entirely different than you first thought:

down X increases your aim by X units.
up X decreases your aim by X units.
forward X does two things:
It increases your horizontal position by X units.
It increases your depth by your aim multiplied by X.

Again note that since you're on a submarine, down and up do the opposite of what you might expect:
"down" means aiming in the positive direction.

Now, the above example does something different:

forward 5 adds 5 to your horizontal position, a total of 5. Because your aim is 0, your
depth does not change.
down 5 adds 5 to your aim, resulting in a value of 5.
forward 8 adds 8 to your horizontal position, a total of 13. Because your aim is 5, your
depth increases by 8*5=40.
up 3 decreases your aim by 3, resulting in a value of 2.
down 8 adds 8 to your aim, resulting in a value of 10.
forward 2 adds 2 to your horizontal position, a total of 15. Because your aim is 10, your
depth increases by 2*10=20 to a total of 60.

After following these new instructions, you would have a horizontal position of 15 and a
depth of 60. (Multiplying these produces 900.)

Using this new interpretation of the commands, calculate the horizontal position and depth you
would have after following the planned course. What do you get if you multiply your
final horizontal position by your final depth?
|#

(defun aoc-day-2-part-2 (path)
  (with-open-file (stream path)
    (loop with depth = 0
          with horiz = 0
          with aim = 0
          for direction = (read stream nil)
          for distance = (read stream nil)
          while direction
          if (string= direction "DOWN")
            do (incf aim distance)
          else if (string= direction "UP")
                 do (decf aim distance)
          else if (string= direction "FORWARD")
                 do (incf horiz distance)
                    (incf depth (* aim distance))
          finally (return (* depth horiz)))))


#|--- Day 3: Binary Diagnostic ---

The submarine has been making some odd creaking noises, so you ask it to produce a diagnostic
report just in case.

The diagnostic report (your puzzle input) consists of a list of binary numbers which, when decoded
properly, can tell you many useful things about the conditions of the submarine. The first
parameter to check is the power consumption.

You need to use the binary numbers in the diagnostic report to generate two new binary numbers
(called the gamma rate and the epsilon rate). The power consumption can then be found by
multiplying the gamma rate by the epsilon rate.

Each bit in the gamma rate can be determined by finding the most common bit in the corresponding
position of all numbers in the diagnostic report. For example, given the following diagnostic
report:

00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010

Considering only the first bit of each number, there are five 0 bits and seven 1 bits. Since the
most common bit is 1, the first bit of the gamma rate is 1.

The most common second bit of the numbers in the diagnostic report is 0, so the second bit of
the gamma rate is 0.

The most common value of the third, fourth, and fifth bits are 1, 1, and 0, respectively, and
so the final three bits of the gamma rate are 110.

So, the gamma rate is the binary number 10110, or 22 in decimal.

The epsilon rate is calculated in a similar way; rather than use the most common bit, the least
common bit from each position is used. So, the epsilon rate is 01001, or 9 in decimal. Multiplying
the gamma rate (22) by the epsilon rate (9) produces the power consumption, 198.

Use the binary numbers in your diagnostic report to calculate the gamma rate and epsilon rate, then
multiply them together. What is the power consumption of the submarine? Be sure to represent your
answer in decimal, not binary. |#

(defun aoc-day-3 ()
  (let ((test #P"~/common-lisp/advent-of-code/src/2021/03a.txt")
        (full #P"~/common-lisp/advent-of-code/src/2021/03.txt"))
    (format t "Day 3:~% Part 1 test: ~A~% Part 1 full: ~A~% Part 2 test: ~A~% Part 2 full: ~A~%"
            (aoc-day-3-part-1 test) (aoc-day-3-part-1 full)
            (aoc-day-3-part-2 test) (aoc-day-3-part-2 full))))


(defun aoc-day-3-part-1 (path)
  (multiple-value-bind (bit-length inputs)
      (load-day-3 path)
    (loop with gamma = 0
          with epsilon = 0
          for index from 0 below bit-length
          for gamma-bit = (find-target inputs index)
          for epsilon-bit = (if (zerop gamma-bit) 1 0)
          do (setf gamma (dpb gamma-bit (byte 1 index) gamma)
                   epsilon (dpb epsilon-bit (byte 1 index) epsilon))
          finally (return (* gamma epsilon)))))


(defun find-target (list index)
  (loop for number in list
        counting (plusp (ldb (byte 1 index) number)) into count
        finally (return (let ((half-length (/ (length list) 2)))
                          (if (or (> count half-length)
                                  (= count half-length))
                              1
                              0)))))

(defun load-day-3 (path)
  (with-open-file (stream path)
    (let* ((bit-length 0)
           (inputs (loop for input = (read-line stream nil)
                        while input
                        do (setf bit-length (max bit-length (length input)))
                         collecting (parse-integer input :radix 2))))
      (values bit-length inputs))))

#|--- Part Two ---

Next, you should verify the life support rating, which can be determined by multiplying the oxygen
generator rating by the CO2 scrubber rating.

Both the oxygen generator rating and the CO2 scrubber rating are values that can be found in your
diagnostic report - finding them is the tricky part. Both values are located using a similar
process that involves filtering out values until only one remains. Before searching for either
rating value, start with the full list of binary numbers from your diagnostic report and consider
just the first bit of those numbers. Then:

Keep only numbers selected by the bit criteria for the type of rating value for which you are
searching. Discard numbers which do not match the bit criteria.
If you only have one number left, stop; this is the rating value for which you are searching.
Otherwise, repeat the process, considering the next bit to the right.

The bit criteria depends on which type of rating value you want to find:

To find oxygen generator rating, determine the most common value (0 or 1) in the current bit
position, and keep only numbers with that bit in that position. If 0 and 1 are equally common,
keep values with a 1 in the position being considered.
To find CO2 scrubber rating, determine the least common value (0 or 1) in the current bit position,
and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values
with a 0 in the position being considered.

For example, to determine the oxygen generator rating value using the same example diagnostic
report from above:

Start with all 12 numbers and consider only the first bit of each number. There are more 1 bits (7)
than 0 bits (5), so keep only the 7 numbers with a 1 in the first position: 11110, 10110, 10111,
10101, 11100, 10000, and 11001.
Then, consider the second bit of the 7 remaining numbers: there are more 0 bits (4) than 1 bits (3),
so keep only the 4 numbers with a 0 in the second position: 10110, 10111, 10101, and 10000.
In the third position, three of the four numbers have a 1, so keep those three: 10110, 10111, and
10101.
In the fourth position, two of the three numbers have a 1, so keep those two: 10110 and 10111.
In the fifth position, there are an equal number of 0 bits and 1 bits (one each). So, to find the
oxygen generator rating, keep the number with a 1 in that position: 10111.
As there is only one number left, stop; the oxygen generator rating is 10111, or 23 in decimal.

Then, to determine the CO2 scrubber rating value from the same example above:

Start again with all 12 numbers and consider only the first bit of each number. There are fewer
0 bits (5) than 1 bits (7), so keep only the 5 numbers with a 0 in the first position: 00100, 01111,
00111, 00010, and 01010.
Then, consider the second bit of the 5 remaining numbers: there are fewer 1 bits (2) than 0 bits
(3), so keep only the 2 numbers with a 1 in the second position: 01111 and 01010.
In the third position, there are an equal number of 0 bits and 1 bits (one each). So, to find
the CO2 scrubber rating, keep the number with a 0 in that position: 01010.
As there is only one number left, stop; the CO2 scrubber rating is 01010, or 10 in decimal.

Finally, to find the life support rating, multiply the oxygen generator rating (23) by the CO2
scrubber rating (10) to get 230.

Use the binary numbers in your diagnostic report to calculate the oxygen generator rating and
CO2 scrubber rating, then multiply them together. What is the life support rating of the submarine?
Be sure to represent your answer in decimal, not binary. |#

(defun aoc-day-3-part-2 (path)
  (multiple-value-bind (bit-length inputs)
      (load-day-3 path)
    (let ((o2-numbers (copy-seq inputs))
          (co2-numbers (copy-seq inputs)))
      (loop for index from (1- bit-length) downto 0
            for o2-target = (find-target o2-numbers index)
            for co2-target = (if (zerop (find-target co2-numbers index)) 1 0)
            when (> (length o2-numbers) 1)
              do (setf o2-numbers (filter-life-support o2-numbers index o2-target))
            when (> (length co2-numbers) 1)
              do (setf co2-numbers (filter-life-support co2-numbers index co2-target)))
      (* (first o2-numbers) (first co2-numbers)))))

(defun filter-life-support (list index target)
  (loop for number in list
       when (= target (ldb (byte 1 index) number))
         collecting number into filtered-list
       finally (return filtered-list)))

#|--- Day 4: Giant Squid ---

You're already almost 1.5km (almost a mile) below the surface of the ocean, already so deep that
you can't see any sunlight. What you can see, however, is a giant squid that has attached itself to
the outside of your submarine.

Maybe it wants to play bingo?

Bingo is played on a set of boards each consisting of a 5x5 grid of numbers. Numbers are chosen at
random, and the chosen number is marked on all boards on which it appears. (Numbers may not appear
                                                                            on all boards.) If all numbers in any row or any column of a board are marked, that board wins.
(Diagonals don't count.)

The submarine has a bingo subsystem to help passengers (currently, you and the giant squid) pass
the time. It automatically generates a random order in which to draw numbers and a random set of
boards (your puzzle input). For example:

7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
8  2 23  4 24
21  9 14 16  7
6 10  3 18  5
1 12 20 15 19

3 15  0  2 22
9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
2  0 12  3  7

After the first five numbers are drawn (7, 4, 9, 5, and 11), there are no winners, but the boards
are marked as follows (shown here adjacent to each other to save space):

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

After the next six numbers are drawn (17, 23, 2, 0, 14, and 21), there are still no winners:

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

Finally, 24 is drawn:

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
1 12 20 15 19        14 21 16 12  6         2  0 12  3  7

At this point, the third board wins because it has at least one complete row or column of marked
numbers (in this case, the entire top row is marked: 14 21 17 24 4).

The score of the winning board can now be calculated. Start by finding the sum of all unmarked
numbers on that board; in this case, the sum is 188. Then, multiply that sum by the number that
was just called when the board won, 24, to get the final score, 188 * 24 = 4512.

To guarantee victory against the giant squid, figure out which board will win first. What will your
final score be if you choose that board?
|#

(defun aoc-day-4 ()
  (let ((test #P"~/common-lisp/advent-of-code/src/2021/04a.txt")
        (full #P"~/common-lisp/advent-of-code/src/2021/04.txt"))
    (format t "Day 4:~% Part 1 test: ~A~% Part 1 full: ~A~% Part 2 test: ~A~% Part 2 full: ~A~%"
            (aoc-day-4-part-1 test) (aoc-day-4-part-1 full)
            (aoc-day-4-part-2 test) (aoc-day-4-part-2 full))))


(defun aoc-day-4-part-1 (path)
  (multiple-value-bind (num-list board-list)
      (load-day-4 path)
    (let ((bits-list (loop for board in board-list
                           collecting (make-array '(5 5) :element-type 'bit))))
      (loop for number in num-list
            do (let ((board-nums (new-number number board-list bits-list)))
                 (when board-nums
                   (loop for board-index in board-nums
                         for finished = (test-for-winner (nth board-index bits-list))
                         when finished
                           do (return-from aoc-day-4-part-1
                                (score-board board-index board-list bits-list number)))))))))

(defun score-board (index board-list bits-list number)
  (loop with board = (nth index board-list)
        with bits = (nth index bits-list)
        for row from 0 below 5
        summing (loop for col from 0 below 5
                      when (zerop (aref bits row col))
                        summing (aref board row col))
          into sum
        finally (return (* number sum))))

(defun new-number (number board-list bits-list)
  (loop for board in board-list
        for bits in bits-list
        for index from 0
        with found = '()
        do (loop for row from 0 below 5
                 do (loop for col from 0 below 5
                          when (= number (aref board row col))
                            do (setf (aref bits row col) 1)
                               (push index found)))
        finally (return found)))

(defun test-for-winner (bits)
  (loop for row from 0 below 5
        do (loop for col from 0 below 5
                 summing (aref bits row col) into row-sum
                 finally (when (= row-sum 5) (return-from test-for-winner t))))
  (loop for col from 0 below 5
        do (loop for row from 0 below 5
                 summing (aref bits row col) into col-sum
                 finally (when (= col-sum 5) (return-from test-for-winner t))))
  nil)



(defun load-day-4 (path)
  (with-open-file (stream path)
    (loop with numbers = (read-line stream nil)
          for input = (read stream nil)
          while input
          collecting input into input-list
          finally (let ((board-list
                          (loop for board-index from 0 below (/ (length input-list) 25)
                                for board = (make-array '(5 5) :element-type 'fixnum)
                                do (loop for row from 0 below 5
                                         do (loop for col from 0 below 5
                                                  do (setf (aref board row col)
                                                           (nth (+ col (* row 5) (* board-index 25))
                                                                input-list))))
                                collecting board))
                        (num-list (loop for number in (split-to-list numbers #\,)
                                       collecting (parse-integer number))))
                    (return (values num-list board-list))))))

(defun split-to-list (string split-char)
  (loop with last-index = -1
        for index from 0 below (length string)
        when (char= (char string index) split-char)
          collecting (subseq string (1+ last-index) index) into result
          and do (setf last-index index)
        finally (return (append result (list (subseq string (1+ last-index)))))))

#|--- Part Two ---

On the other hand, it might be wise to try a different strategy: let the giant squid win.

You aren't sure how many bingo boards a giant squid could play at once, so rather than waste
time counting its arms, the safe thing to do is to figure out which board will win last and
choose that one. That way, no matter which boards it picks, it will win for sure.

In the above example, the second board is the last to win, which happens after 13 is eventually
called and its middle column is completely marked. If you were to keep playing until
this point, the second board would have a sum of unmarked numbers equal to 148 for a
final score of 148 * 13 = 1924.

Figure out which board will win last. Once it wins, what would its final score be? |#

(defun aoc-day-4-part-2 (path)
  (multiple-value-bind (num-list board-list)
      (load-day-4 path)
    (let ((bits-list (loop for board in board-list
                           collecting (make-array '(5 5) :element-type 'bit))))
      (loop for number in num-list
            do (let ((board-nums (new-number number board-list bits-list)))
                 (when board-nums
                   (loop for board-index in board-nums
                         for finished = (test-for-winner (nth board-index bits-list))
                         when finished
                           do (let ((finished-board (nth board-index board-list))
                                    (finished-bits (nth board-index bits-list)))
                                (setf board-list (remove finished-board board-list)
                                      bits-list (remove finished-bits bits-list))
                                (when (= 0 (length board-list))
                                  (return-from aoc-day-4-part-2
                                    (score-board 0
                                                 (list finished-board)
                                                 (list finished-bits)
                                                 number)))))))))))


#|--- Day 5: Hydrothermal Venture ---

You come across a field of hydrothermal vents on the ocean floor! These vents constantly produce
large, opaque clouds, so it would be best to avoid them if possible.

They tend to form in lines; the submarine helpfully produces a list of nearby lines of vents
(your puzzle input) for you to review. For example:

0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2

Each line of vents is given as a line segment in the format x1,y1 -> x2,y2 where x1,y1 are the
coordinates of one end the line segment and x2,y2 are the coordinates of the other end. These
line segments include the points at both ends. In other words:

An entry like 1,1 -> 1,3 covers points 1,1, 1,2, and 1,3.
An entry like 9,7 -> 7,7 covers points 9,7, 8,7, and 7,7.

For now, only consider horizontal and vertical lines: lines where either x1 = x2 or y1 = y2.

So, the horizontal and vertical lines from the above list would produce the following diagram:

.......1..
..1....1..
..1....1..
.......1..
.112111211
..........
..........
..........
..........
222111....

In this diagram, the top left corner is 0,0 and the bottom right corner is 9,9. Each position
is shown as the number of lines which cover that point or . if no line covers that point. The
top-left pair of 1s, for example, comes from 2,2 -> 2,1; the very bottom row is formed by the
overlapping lines 0,9 -> 5,9 and 0,9 -> 2,9.

To avoid the most dangerous areas, you need to determine the number of points where at least
two lines overlap. In the above example, this is anywhere in the diagram with a 2 or larger -
a total of 5 points.

Consider only horizontal and vertical lines. At how many points do at least two lines overlap?
|#

(defun aoc-day-5 ()
  (let ((test #P"~/common-lisp/advent-of-code/src/2021/05a.txt")
        (full #P"~/common-lisp/advent-of-code/src/2021/05.txt"))
    (format t "Day 5:~% Part 1 test: ~A~% Part 1 full: ~A~% Part 2 test: ~A~% Part 2 full: ~A~%"
            (aoc-day-5-part-1 test) (aoc-day-5-part-1 full)
            (aoc-day-5-part-2 test) (aoc-day-5-part-2 full))))

(defun aoc-day-5-part-1 (path)
  (multiple-value-bind (min-x-width min-y-width)
      (find-maxes-in-file path)
    (let ((field (make-array (list (1+ min-x-width) (1+ min-y-width)) :element-type 'fixnum
                                               :adjustable nil :fill-pointer nil)))
      (fill-field field path)
      (count-overlaps field))))

(defun count-overlaps (field)
  (loop with count = 0
        for x from 0 below (array-dimension field 0)
        do (loop for y from 0 below (array-dimension field 1)
                 when (> (aref field x y) 1)
                   do (incf count))
        finally (return count)))


(defun fill-field (field path)
  (with-open-file (stream path)
    (loop for line = (read-line stream nil)
          while line
          do (let ((x1 (parse-integer (split-from-start line ",")))
                   (y1 (parse-integer (split-between line "," " -> ")))
                   (x2 (parse-integer (split-between line " -> " ",")))
                   (y2 (parse-integer (split-to-end line ","))))
               (fill-field-line field x1 y1 x2 y2)))))

(defun fill-field-line (field x1 y1 x2 y2)
  (cond ((= x1 x2)
         (if (> y2 y1)
             (loop for y from y1 to y2
                   do (incf (aref field x1 y)))
             (loop for y from y2 to y1
                   do (incf (aref field x1 y)))))
        ((= y1 y2)
         (if (> x2 x1)
             (loop for x from x1 to x2
                   do (incf (aref field x y1)))
             (loop for x from x2 to x1
                   do (incf (aref field x y1)))))))


(defun find-maxes-in-file (path)
  (with-open-file (stream path)
    (loop with max-x = 0
          with max-y = 0
          for line = (read-line stream nil)
          while line
          do (let ((x1 (parse-integer (split-from-start line ",")))
                   (y1 (parse-integer (split-between line "," " -> ")))
                   (x2 (parse-integer (split-between line " -> " ",")))
                   (y2 (parse-integer (split-to-end line ","))))
               (when (> x1 max-x) (setf max-x x1))
               (when (> x2 max-x) (setf max-x x2))
               (when (> y1 max-y) (setf max-y y1))
               (when (> y2 max-y) (setf max-y y2)))
          finally (return (values max-x max-y)))))

(defun split-from-start (string target-end)
  "Return the subseq of the string from the start to, but not including, the target-end."
  (loop with target-length = (length target-end)
        for index from 0 below (- (length string) target-length)
        when (string= (subseq string index (+ index target-length)) target-end)
          do (return (subseq string 0 index))))

(defun split-between (string target-start target-end)
  "Return the subseq of the string between but not including the targets."
  (loop with string-length = (length string)
        with start-length = (length target-start)
        for start-index from 0
          below (- string-length start-length)
        when (string= (subseq string start-index (+ start-index start-length)) target-start)
          do (loop with end-length = (length target-end)
                   for end-index from (+ start-index start-length)
                     below (- string-length end-length)
                   when (string= (subseq string end-index (+ end-index end-length)) target-end)
                     do (return-from split-between (subseq string
                                                           (+ start-index start-length)
                                                           end-index)))))

(defun split-to-end (string target-start)
  "Return the subseq of the string from but not including the target-start, to the end. "
  (loop with target-length = (length target-start)
        for index from (- (length string) target-length 1) downto 0
        when (string= (subseq string index (+ index target-length)) target-start)
          do (return (subseq string (+ target-length index)))))


#|--- Part Two ---

Unfortunately, considering only horizontal and vertical lines doesn't give you the full
picture; you need to also consider diagonal lines.

Because of the limits of the hydrothermal vent mapping system, the lines in your list will
only ever be horizontal, vertical, or a diagonal line at exactly 45 degrees. In other words:

An entry like 1,1 -> 3,3 covers points 1,1, 2,2, and 3,3.
An entry like 9,7 -> 7,9 covers points 9,7, 8,8, and 7,9.

Considering all lines from the above example would now produce the following diagram:

1.1....11.
.111...2..
..2.1.111.
...1.2.2..
.112313211
...1.2....
..1...1...
.1.....1..
1.......1.
222111....

You still need to determine the number of points where at least two lines overlap. In the
above example, this is still anywhere in the diagram with a 2 or larger - now a total of 12 points.

Consider all of the lines. At how many points do at least two lines overlap?
|#

(defun aoc-day-5-part-2 (path)
  (multiple-value-bind (min-x-width min-y-width)
      (find-maxes-in-file path)
    (let ((field (make-array (list (1+ min-x-width) (1+ min-y-width)) :element-type 'fixnum
                                               :adjustable nil :fill-pointer nil)))
      (fill-field-diag field path)
      (count-overlaps field))))

(defun fill-field-diag (field path)
  (with-open-file (stream path)
    (loop for line = (read-line stream nil)
          while line
          do (let ((x1 (parse-integer (split-from-start line ",")))
                   (y1 (parse-integer (split-between line "," " -> ")))
                   (x2 (parse-integer (split-between line " -> " ",")))
                   (y2 (parse-integer (split-to-end line ","))))
               (fill-field-line-diag field x1 y1 x2 y2)))))

(defun fill-field-line-diag (field x1 y1 x2 y2)
  (cond ((= x1 x2)
         (if (> y2 y1)
             (loop for y from y1 to y2
                   do (incf (aref field x1 y)))
             (loop for y from y2 to y1
                   do (incf (aref field x1 y)))))
        ((= y1 y2)
         (if (> x2 x1)
             (loop for x from x1 to x2
                   do (incf (aref field x y1)))
             (loop for x from x2 to x1
                   do (incf (aref field x y1)))))
        ((= (abs (- x1 x2))
            (abs (- y1 y2)))
         (cond ((and (> x1 x2)
                     (> y1 y2))
                (loop for x from x2 to x1
                      for y from y2 to y1
                        do (incf (aref field x y))))
               ((and (< x1 x2)
                     (< y1 y2))
                (loop for x from x1 to x2
                      for y from y1 to y2
                        do (incf (aref field x y))))
               ((> x1 x2)
                (loop for x from x1 downto x2
                      for y from y1 to y2
                        do (incf (aref field x y))))
               (t (loop for x from x1 to x2
                        for y from y1 downto y2
                        do (incf (aref field x y))))))))





#|--- Day 6: Lanternfish ---

The sea floor is getting steeper. Maybe the sleigh keys got carried this way?

A massive school of glowing lanternfish swims past. They must spawn quickly to reach such large
numbers - maybe exponentially quickly? You should model their growth rate to be sure.

Although you know nothing about this specific species of lanternfish, you make some guesses about
their attributes. Surely, each lanternfish creates a new lanternfish once every 7 days.

However, this process isn't necessarily synchronized between every lanternfish - one lanternfish
might have 2 days left until it creates another lanternfish, while another might have 4. So, you
can model each fish as a single number that represents the number of days until it creates a
new lanternfish.

Furthermore, you reason, a new lanternfish would surely need slightly longer before it's capable
of producing more lanternfish: two more days for its first cycle.

So, suppose you have a lanternfish with an internal timer value of 3:

After one day, its internal timer would become 2.
After another day, its internal timer would become 1.
After another day, its internal timer would become 0.
After another day, its internal timer would reset to 6, and it would create a new lanternfish
with an internal timer of 8.
After another day, the first lanternfish would have an internal timer of 5, and the second
lanternfish would have an internal timer of 7.

A lanternfish that creates a new fish resets its timer to 6, not 7 (because 0 is included as a
                                                                    valid timer value). The new lanternfish starts with an internal timer of 8 and does not start
counting down until the next day.

Realizing what you're trying to do, the submarine automatically produces a list of the ages of
several hundred nearby lanternfish (your puzzle input). For example, suppose you were given
the following list:

3,4,3,1,2

This list means that the first fish has an internal timer of 3, the second fish has an internal
timer of 4, and so on until the fifth fish, which has an internal timer of 2. Simulating these
fish over several days would proceed as follows:

Initial state: 3,4,3,1,2
After  1 day:  2,3,2,0,1
After  2 days: 1,2,1,6,0,8
After  3 days: 0,1,0,5,6,7,8
After  4 days: 6,0,6,4,5,6,7,8,8
After  5 days: 5,6,5,3,4,5,6,7,7,8
After  6 days: 4,5,4,2,3,4,5,6,6,7
After  7 days: 3,4,3,1,2,3,4,5,5,6
After  8 days: 2,3,2,0,1,2,3,4,4,5
After  9 days: 1,2,1,6,0,1,2,3,3,4,8
After 10 days: 0,1,0,5,6,0,1,2,2,3,7,8
After 11 days: 6,0,6,4,5,6,0,1,1,2,6,7,8,8,8
After 12 days: 5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8
After 13 days: 4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8
After 14 days: 3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8
After 15 days: 2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7
After 16 days: 1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8
After 17 days: 0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8
After 18 days: 6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8

Each day, a 0 becomes a 6 and adds a new 8 to the end of the list, while each other number
decreases by 1 if it was present at the start of the day.

In this example, after 18 days, there are a total of 26 fish. After 80 days, there would
be a total of 5934.

Find a way to simulate lanternfish. How many lanternfish would there be after 80 days?
|#

(defun aoc-day-6 ()
  (let ((test #P"~/common-lisp/advent-of-code/src/2021/06a.txt")
        (full #P"~/common-lisp/advent-of-code/src/2021/06.txt"))
    (format t "Day 6:~% Part 1 test: ~A~% Part 1 full: ~A~% Part 2 test: ~A~% Part 2 full: ~A~%"
            (aoc-day-6-part-1 test) (aoc-day-6-part-1 full)
            (aoc-day-6-part-2 test) (aoc-day-6-part-2 full))))

(defun aoc-day-6-part-1 (path)
  (calculate-fish (load-aoc-day-6 path) 80))

(defun load-aoc-day-6 (path)
  (with-open-file (stream path)
    (loop with fish = (make-array 9 :element-type 'fixnum :adjustable nil :fill-pointer nil)
          with zero-code = (char-code #\0)
          for char = (read-char stream nil)
          while char
          when (digit-char-p char)
            do (incf (aref fish (- (char-code char) zero-code)))
          finally (return fish))))

(defun calculate-fish (fish days)
  (loop repeat days
        do (loop with 0s = (aref fish 0)
                 for age from 0 to 7
                 do (setf (aref fish age) (aref fish (1+ age)))
                 finally (setf (aref fish 8) 0s
                               (aref fish 6) (+ (aref fish 6) 0s))))
  (loop for count across fish
        summing count))



#|--- Part Two ---

Suppose the lanternfish live forever and have unlimited food and space. Would they take
over the entire ocean?

After 256 days in the example above, there would be a total of 26984457539 lanternfish!

How many lanternfish would there be after 256 days?
|#


(defun aoc-day-6-part-2 (path)
  (calculate-fish (load-aoc-day-6 path) 256))

#|--- Day 7: The Treachery of Whales ---

A giant whale has decided your submarine is its next meal, and it's much faster than you are.
There's nowhere to run!

Suddenly, a swarm of crabs (each in its own tiny submarine - it's too deep for them otherwise)
zooms in to rescue you! They seem to be preparing to blast a hole in the ocean floor; sensors
indicate a massive underground cave system just beyond where they're aiming!

The crab submarines all need to be aligned before they'll have enough power to blast a large
enough hole for your submarine to get through. However, it doesn't look like they'll be aligned
before the whale catches you! Maybe you can help?

There's one major catch - crab submarines can only move horizontally.

You quickly make a list of the horizontal position of each crab (your puzzle input). Crab
submarines have limited fuel, so you need to find a way to make all of their horizontal positions
match while requiring them to spend as little fuel as possible.

For example, consider the following horizontal positions:

16,1,2,0,4,2,7,1,2,14

This means there's a crab with horizontal position 16, a crab with horizontal position 1, and so on.

Each change of 1 step in horizontal position of a single crab costs 1 fuel. You could choose any
horizontal position to align them all on, but the one that costs the least fuel is horizontal
position 2:

    Move from 16 to 2: 14 fuel
    Move from 1 to 2: 1 fuel
    Move from 2 to 2: 0 fuel
    Move from 0 to 2: 2 fuel
    Move from 4 to 2: 2 fuel
    Move from 2 to 2: 0 fuel
    Move from 7 to 2: 5 fuel
    Move from 1 to 2: 1 fuel
    Move from 2 to 2: 0 fuel
    Move from 14 to 2: 12 fuel

This costs a total of 37 fuel. This is the cheapest possible outcome; more expensive outcomes
include aligning at position 1 (41 fuel), position 3 (39 fuel), or position 10 (71 fuel).

Determine the horizontal position that the crabs can align to using the least fuel possible.
How much fuel must they spend to align to that position?

|#

(defun aoc-day-7 ()
  (let ((test #P"~/common-lisp/advent-of-code/src/2021/07a.txt")
        (full #P"~/common-lisp/advent-of-code/src/2021/07.txt"))
    (format t "Day 7:~% Part 1 test: ~A~% Part 1 full: ~A~% Part 2 test: ~A~% Part 2 full: ~A~%"
            (aoc-day-7-part-1 test) (aoc-day-7-part-1 full)
            (aoc-day-7-part-2 test) (aoc-day-7-part-2 full))))

(defun aoc-day-7-part-1 (path)
  (let* ((list (load-day-7 path))
         (median (median list))
         (buffer 20)) ; answer is likely near the median, this is how far on each side to search
    (loop with min-distance = (reduce #'+ list)
          with min-position = nil
          for position from (- median buffer) to (+ median buffer)
          do (let ((distance (calculate-horizontal-sum list position)))
               (when (< distance min-distance)
                 (setf min-distance distance
                       min-position position)))
          finally (return min-distance))))

(defun calculate-horizontal-sum (list position)
  (loop for crab in list
        summing (abs (- crab position))))

(defun load-day-7 (path)
  (with-open-file (stream path)
    (let ((input (read-line stream)))
       (sort (split-to-int-list input #\,) #'<))))

(defun split-to-int-list (string char)
  (mapcar #'parse-integer (split-to-list string char)))

(defun mean (list)
  (floor (reduce #'+ list) (length list)))

(defun median (list)
  (nth (floor (length list) 2) list))

#|--- Part Two ---

The crabs don't seem interested in your proposed solution. Perhaps you misunderstand crab
engineering?

As it turns out, crab submarine engines don't burn fuel at a constant rate. Instead, each
change of 1 step in horizontal position costs 1 more unit of fuel than the last: the first
step costs 1, the second step costs 2, the third step costs 3, and so on.

As each crab moves, moving further becomes more expensive. This changes the best horizontal
position to align them all on; in the example above, this becomes 5:

Move from 16 to 5: 66 fuel
Move from 1 to 5: 10 fuel
Move from 2 to 5: 6 fuel
Move from 0 to 5: 15 fuel
Move from 4 to 5: 1 fuel
Move from 2 to 5: 6 fuel
Move from 7 to 5: 3 fuel
Move from 1 to 5: 10 fuel
Move from 2 to 5: 6 fuel
Move from 14 to 5: 45 fuel

This costs a total of 168 fuel. This is the new cheapest possible outcome; the old alignment
position (2) now costs 206 fuel instead.

Determine the horizontal position that the crabs can align to using the least fuel possible so
they can make you an escape route! How much fuel must they spend to align to that position?
|#

(defun aoc-day-7-part-2 (path)
  (let* ((list (load-day-7 path))
         (median (median list))
         (mean (mean list)))
    ;; answer is probably between median and mean, or past the mean from the median, so start
    ;; at the median and keep moving over until we pass the minimum.
    (if (< median mean)
        (loop with min-distance
              with min-position
              for position from median
              for distance = (calculate-horizontal-sum-2 list position)
              until (and min-distance (> distance min-distance))
              when (or (null min-distance) (< distance min-distance))
                do (setf min-position position
                         min-distance distance)
              finally (return min-distance))
        (loop with min-distance
              with min-position
              for position downfrom median
              for distance = (calculate-horizontal-sum-2 list position)
              until (and min-distance (> distance min-distance))
              when (or (null min-distance) (< distance min-distance))
                do (setf min-position position
                         min-distance distance)
              finally (return min-distance)))))

(defun calculate-horizontal-sum-2 (list position)
  (loop for crab in list
        summing (if (< crab position)
                    (loop for pos from crab to position
                          for fuel from 0
                          summing fuel)
                    (loop for pos downfrom crab to position
                          for fuel from 0
                          summing fuel))))


#|--- Day 8: Seven Segment Search ---

You barely reach the safety of the cave when the whale smashes into the cave mouth, collapsing it.
Sensors indicate another exit to this cave at a much greater depth, so you have no choice but to
press on.

As your submarine slowly makes its way through the cave system, you notice that the four-digit
seven-segment displays in your submarine are malfunctioning; they must have been damaged during
the escape. You'll be in a lot of trouble without them, so you'd better figure out what's wrong.

Each digit of a seven-segment display is rendered by turning on or off any of seven segments named
a through g:

0:      1:      2:      3:      4:
aaaa    ....    aaaa    aaaa    ....
b    c  .    c  .    c  .    c  b    c
b    c  .    c  .    c  .    c  b    c
....    ....    dddd    dddd    dddd
e    f  .    f  e    .  .    f  .    f
e    f  .    f  e    .  .    f  .    f
gggg    ....    gggg    gggg    ....

5:      6:      7:      8:      9:
aaaa    aaaa    aaaa    aaaa    aaaa
b    .  b    .  .    c  b    c  b    c
b    .  b    .  .    c  b    c  b    c
dddd    dddd    ....    dddd    dddd
.    f  e    f  .    f  e    f  .    f
.    f  e    f  .    f  e    f  .    f
gggg    gggg    ....    gggg    gggg

So, to render a 1, only segments c and f would be turned on; the rest would be off. To render a 7,
only segments a, c, and f would be turned on.

The problem is that the signals which control the segments have been mixed up on each display. The
submarine is still trying to display numbers by producing output on signal wires a through g, but
those wires are connected to segments randomly. Worse, the wire/segment connections are mixed up
separately for each four-digit display! (All of the digits within a display use the same)
connections, though.

So, you might know that only signal wires b and g are turned on, but that doesn't mean segments b
and g are turned on: the only digit that uses two segments is 1, so it must mean segments c and f
are meant to be on. With just that information, you still can't tell which wire (b/g) goes to which
segment (c/f). For that, you'll need to collect more information.

For each display, you watch the changing signals for a while, make a note of all ten unique signal
patterns you see, and then write down a single four digit output value (your puzzle input). Using
the signal patterns, you should be able to work out which pattern corresponds to which digit.

For example, here is what you might see in a single entry in your notes:

acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf

(The entry is wrapped here to two lines so it fits); in your notes, it will all be on a single
line.

Each entry consists of ten unique signal patterns, a | delimiter, and finally the four digit output
value. Within an entry, the same wire/segment connections are used but you don't know what the
connections actually are. The unique signal patterns correspond to the ten different ways the
submarine tries to render a digit using the current wire/segment connections. Because 7 is the only
digit that uses three segments, dab in the above example means that to render a 7, signal lines d,
a, and b are on. Because 4 is the only digit that uses four segments, eafb means that to render a 4,
signal lines e, a, f, and b are on.

Using this information, you should be able to work out which combination of signal wires corresponds
to each of the ten digits. Then, you can decode the four digit output value. Unfortunately, in the
above example, all of the digits in the output value (cdfeb fcadb cdfeb cdbaf) use five segments and
are more difficult to deduce.

For now, focus on the easy digits. Consider this larger example:

be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce

Because the digits 1, 4, 7, and 8 each use a unique number of segments, you should be able to tell
which combinations of signals correspond to those digits. Counting only digits in the output values
(the part after | on each line), in the above example, there are 26 instances of digits that use a
unique number of segments (highlighted above).

In the output values, how many times do digits 1, 4, 7, or 8 appear? |#


(defun aoc-day-8 ()
  (let ((test #P"~/common-lisp/advent-of-code/src/2021/08a.txt")
        (full #P"~/common-lisp/advent-of-code/src/2021/08.txt"))
    (format t "Day 8:~% Part 1 test: ~A~% Part 1 full: ~A~% Part 2 test: ~A~% Part 2 full: ~A~%"
            (aoc-day-8-part-1 test) (aoc-day-8-part-1 full)
            (aoc-day-8-part-2 test) (aoc-day-8-part-2 full))))

(defun aoc-day-8-part-1 (path)
  (with-open-file (stream path)
    (loop for line = (read-line stream nil)
          while line
          summing (let ((output (split-to-end line "| ")))
                    (loop for display in (split-to-list output #\SPACE)
                          counting (let ((length (length display)))
                                     (or (= length 2)
                                         (= length 3)
                                         (= length 4)
                                         (= length 7))))))))

#|--- Part Two ---

Through a little deduction, you should now be able to determine the remaining digits. Consider again
the first example above:

acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf

After some careful analysis, the mapping between signal wires and segments only make sense in the
following configuration:

dddd
e    a
e    a
ffff
g    b
g    b
cccc

So, the unique signal patterns would correspond to the following digits:

acedgfb: 8
cdfbe: 5
gcdfa: 2
fbcad: 3
dab: 7
cefabd: 9
cdfgeb: 6
eafb: 4
cagedb: 0
ab: 1

Then, the four digits of the output value can be decoded:

cdfeb: 5
fcadb: 3
cdfeb: 5
cdbaf: 3

Therefore, the output value for this entry is 5353.

Following this same process for each entry in the second, larger example above, the output value of
each entry can be determined:

fdgacbe cefdb cefbgd gcbe: 8394
fcgedb cgb dgebacf gc: 9781
cg cg fdcagb cbg: 1197
efabcd cedba gadfec cb: 9361
gecf egdcabf bgf bfgea: 4873
gebdcfa ecba ca fadegcb: 8418
cefg dcbef fcge gbcadfe: 4548
ed bcgafe cdgba cbgef: 1625
gbdfcae bgc cg cgb: 8717
fgae cfgab fg bagce: 4315

Adding all of the output values in this larger example produces 61229.

For each entry, determine all of the wire/segment connections and decode the four-digit output
values. What do you get if you add up all of the output values?
|#

(defun aoc-day-8-part-2 (path)
  (with-open-file (stream path)
    (loop for line = (read-line stream nil)
          while line)))
         ;; summing  (let ((output (split-to-end line " | "))
         ;;                (signals (split-from-start line " | ")))))))

(defun day-8-split (string)
  (let ((output (split-to-end string " | "))
        (signals (split-from-start string " | ")))))

(defvar test
  "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")






























































