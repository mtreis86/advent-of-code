(defpackage :aoc2024
  (:use :cl))

(in-package :aoc2024)


#|
(defun aoc ()
  (labels ((solve (path)
             (with-open-file (stream path)
               (loop for line = (read-line stream nil)
                     while line))))
    (format t "01a test: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/test.txt"))
    (format t "01a real: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/input.txt"))))
|#

(defun aoc01a ()
  (labels ((solve (path)
             (with-open-file (stream path)
               (loop for line = (read-line stream nil)
                     while line
                     for index = (position #\SPACE line)
                     collecting (parse-integer line :end index) into list-a
                     collecting (parse-integer line :start (1+ index)) into list-b
                     finally (return (reduce #'+ (mapcar (lambda (x y) (abs (- x y)))
                                                         (sort list-a #'<)
                                                         (sort list-b #'<))))))))
    (format t "01a test: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/test01.txt"))
    (format t "01a real: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/input01.txt"))))

(defun aoc01b ()
  (labels ((solve (path)
             (with-open-file (stream path)
               (loop with nums-a = (make-hash-table)
                     with nums-b = (make-hash-table)
                     for line = (read-line stream nil)
                     while line
                     for index = (position #\SPACE line)
                     do (let ((a (parse-integer line :end index))
                              (b (parse-integer line :start (1+ index))))
                          (if (gethash a nums-a)
                              (incf (gethash a nums-a))
                              (setf (gethash a nums-a) 1))
                          (if (gethash b nums-b)
                              (incf (gethash b nums-b))
                              (setf (gethash b nums-b) 1)))
                     finally (return (loop for k being the hash-key of nums-a using (hash-value v)
                                           summing (* k v (let ((b (gethash k nums-b)))
                                                            (if b
                                                                b
                                                                0)))))))))
    (format t "01a test: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/test01.txt"))
    (format t "01a real: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/input01.txt"))))

(defun aoc02a ()
  (labels ((safe-p (list)
             (loop for level in list
                   for next in (rest list)
                   for diff = (abs (- level next))
                   for direction = (if (> next level)
                                       '+
                                       '-)
                   with prev-dir
                   when (or (= diff 0)
                            (> diff 3)
                            (and prev-dir
                                 (not (eq direction prev-dir))))
                        do (return nil)
                   do (setf prev-dir direction)
                   finally (return t)))
           (solve (path)
             (with-open-file (stream path)
               (loop for line = (read-line stream nil)
                     while line
                     when (safe-p (number-string-to-list line))
                       summing 1 into sum
                     finally (return sum)))))
    (format t "02a test: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/test02.txt"))
    (format t "02a real: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/input02.txt"))))

(defun aoc02b ()
  (labels ((safe-p (list)
             (loop for level in list
                   for next in (rest list)
                   for diff = (abs (- level next))
                   for direction = (if (> next level)
                                       '+
                                       '-)
                   with prev-dir
                   when (or (= diff 0)
                            (> diff 3)
                            (and prev-dir
                                 (not (eq prev-dir direction))))
                     do (return nil)
                   do (setf prev-dir direction)
                   finally (return t)))
           (solve (path)
             (with-open-file (stream path)
               (loop with sum = 0
                     for line = (read-line stream nil)
                     while line
                     do (let ((list (number-string-to-list line)))
                          (when (or (safe-p list)
                                    (loop for item in list
                                          for index from 0
                                          when (safe-p (remove-nth list index))
                                            do (return t)
                                          finally (return nil)))
                            (incf sum)))
                     finally (return sum)))))
    (format t "02b test: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/test02.txt"))
    (format t "02b real: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/input02.txt"))))

(defun aoc03a ()
  (labels ((solve (path)
             (with-open-file (stream path))))
    (format t "01a test: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/test03.txt"))))
    ;; (format t "01a real: ~A~%" (solve #P"~/common-lisp/puzzles/advent-of-code/src/2024/input03.txt"))))

;; xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))
#|
(defmatch mul
  (match-string "mul(")
  (match-number 'num-a)
  (match-char #\,)
  (match-number 'num-b)
  (match-char #\)))

|#





#|
(defstruct match
  ;; index is where the match starts in the string
  ;; subseq is the chunk of the string that matched
  ;; retvals is an alist of the matched symbol and the contents of the match
  index subseq retvals)

;; (defpat mul "mul(" (mul-a #'numberp nil #'parse-integer) "," (mul-b #'numberp nil #parse-integer) ")")


Pattern options
Strings match the whole string
_ matches any one char
? matches anything in between, or before or after if the first or last part of the pattern
fn match is a list of (symbol function length retval-fn)
  length are constraints on the function length:
    number matches the length exactly
    (> number) for minimum length
    (< number) for max length
    nil matches any length so long as the function keeps matching
  retval-fn takes the whole match of for that subseq and applies the fn to it as the thing to store
    nil returns the string
    eg parse-integer combines the digits into the base-10 number


;; (match-all string mul)

(defun match-all (string pattern)
  "Find all matches to the pattern within the string. Returns a list of match structures."
  (loop for match = (match-one string pattern)
          then (match-one (subseq string (match-index match)) pattern)
        collecting match))

(defun match-one (string pattern)
  "Find the first instance of the pattern within the string. Returns a match structure."
  (loop for char across string
        for index from 0
        with matches = (make-deque)
        when (match-start char)
          (deque-push (make-match :index pos))
        while))


(defun match-string (string to-match)
  (string= string to-match))

(defun match-fn (char fn)
  ())

(defun pos-in-string (to-find string)
  "Find the position of to-find within the string, returning the position if found and nil otherwise."
  (loop for char across string
        for index from 0
        with to-find-index = 0
        with pos = 0
        do (if (char= char (char to-find to-find-index))
             (progn (when (= to-find-index 0)
                       (setf pos index))
                    (if (= (1- (length to-find)) to-find-index)
                       (return pos)
                       (incf to-find-index)))
             (unless (zerop to-find-index)
               (setf to-find-index 0)))))

|#

(defun remove-nth (list nth)
  "Remove the nth item from the list, returning a new list."
  (loop for item in list
        for index from 0
        unless (= nth index)
          collecting item))

(defun number-string-to-list (string)
  "Assuming the string is a series of numbers, return those as a list"
  (loop for char across string
        for end from 0
        with start = 0
        when (and (char= char #\SPACE)
                  (not (= start end)))
          collecting (parse-integer string :start start :end end) into list
        and do (setf start (1+ end))
        finally (return (append list (list (parse-integer string :start start))))))

(defstruct (que (:constructor %make-que))
  head tail)

(defun make-que (&rest items)
  "Make a new que object. Any items are included in the que in order of the first in the list
   will be the first to get dequed."
  (%make-que :head items :tail (last items)))

(defun enque (item que)
  "Add a new item to the back of the que."
  (if (que-head que)
      (progn (rplacd (que-tail que) (cons item nil))
             (setf (que-tail que) (cdr (que-tail que))))
      (let ((new (cons item nil)))
        (setf (que-head que) new
              (que-tail que) new)))
  (que-head que))

(defun deque (que)
  "Pop the first item off the front of the que and return it."
  (if (null (que-head que))
      (error "Que is empty: ~A" que)
      (prog1 (car (que-head que))
        (setf (que-head que) (cdr (que-head que)))
        (unless (que-head que)
          (setf (que-tail que) nil)))))

